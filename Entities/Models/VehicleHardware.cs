﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Entities.Models
{
    public class VehicleHardware
    {

        [Key]
        public Guid Id { get; set; }

        public string HardwareId { get; set; }

        public string Hardware { get; set; }
    }
}
