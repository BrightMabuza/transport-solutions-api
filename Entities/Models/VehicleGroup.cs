﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Entities.Models
{
    public class VehicleGroup
    {
        [Key]
        public Guid Id { get; set; }

        public string GroupId { get; set; }

        public long FleetId { get; set; }

        public string GroupName { get; set; }

        public string Description { get; set; }

        public string InoId { get; set; }
    }
}
