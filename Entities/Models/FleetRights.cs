﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Entities.Models
{
    public class FleetRights
    {
        [Key]
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public string Service { get; set; }
    }
}
