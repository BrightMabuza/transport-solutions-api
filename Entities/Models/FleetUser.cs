﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    [Table("fleetUser")]
    public class FleetUser
    {
        [Key]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "FleetId is required")]
        public long FleetId { get; set; }

        [Required(ErrorMessage = "Fleet Name is required")]
        [StringLength(60, ErrorMessage = "Fleet Name can't be longer than 60 characters")]
        public string Fleet { get; set; }

        [Required(ErrorMessage = "Username is required")]
        [StringLength(60, ErrorMessage = "Username can't be longer than 60 characters")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [StringLength(60, ErrorMessage = "Password can't be longer than 60 characters")]
        public string Password { get; set; }
    }
}
