﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Entities.Models
{
    public class FleetProperties
    {
        [Key]
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }
    }
}