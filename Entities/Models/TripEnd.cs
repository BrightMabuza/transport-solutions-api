﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Entities.Models
{
    public class TripEnd
    {
        [Key]
        public Guid Id { get; set; }

        public double Mileage { get; set; }

        public string VehicleTimeStamp { get; set; }

        public virtual Position Position { get; set; }
    }
}
