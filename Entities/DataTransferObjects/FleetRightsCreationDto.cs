﻿using System.ComponentModel.DataAnnotations;

namespace Entities.DataTransferObjects
{
    public class FleetRightsCreationDto
    {
        public string Name { get; set; }

        public string Type { get; set; }

        public string Service { get; set; }
    }
}
