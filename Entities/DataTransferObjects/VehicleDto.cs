﻿using System;
using System.Collections.Generic;

namespace Entities.DataTransferObjects
{
    public class VehicleDto
    {
        public Guid Id { get; set; }

        public string Chassis { get; set; }

        public long FleetId { get; set; }

        public string INSMSISDN { get; set; }

        public string Registration { get; set; }

        public string Status { get; set; }

        public string VehicleNumber { get; set; }

        public string VehicleType { get; set; }

        public string EndTime { get; set; }

        public string StartTime { get; set; }

        public string RegistrationDate { get; set; }

        public int TelematicGroup { get; set; }

        public bool TelematicGroupSpecified { get; set; }

        public string InoId { get; set; }

        public virtual ICollection<VehicleHardwareDto> VehicleHardware { get; set; }

        public virtual List<string> VehicleGroupId { get; set; }

        public virtual ICollection<VehiclePropertiesDto> VehicleProperties { get; set; }
    }
}
