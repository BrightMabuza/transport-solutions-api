﻿namespace Entities.DataTransferObjects
{
    public class VehicleHardwareCreationDto
    {
        public string HardwareId { get; set; }

        public string Hardware { get; set; }
    }
}
