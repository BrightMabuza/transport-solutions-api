﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Entities.DataTransferObjects.Dto
{
    public class TripEndDto
    {
        [Key]
        public Guid Id { get; set; }

        public double Mileage { get; set; }

        public string VehicleTimeStamp { get; set; }

        public virtual PositionDto Position { get; set; }
    }
}
