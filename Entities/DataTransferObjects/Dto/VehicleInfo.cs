﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Entities.DataTransferObjects.Dto
{
    public class VehicleInfoDto
    {
        [Key]
        public Guid Id { get; set; }

        public long vehicleId { get; set; }

        public int IgnitionStatus { get; set; }

        public bool IgnitionStatusSpecified { get; set; }

        public string MotorStatus { get; set; }

        public bool MotorStatusSpecified { get; set; }

        public string TrailerName { get; set; }
    }
}
