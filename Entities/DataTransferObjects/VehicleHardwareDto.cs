﻿using System;

namespace Entities.DataTransferObjects
{
    public class VehicleHardwareDto
    {
        public Guid Id { get; set; }

        public string HardwareId { get; set; }

        public string Hardware { get; set; }
    }
}
