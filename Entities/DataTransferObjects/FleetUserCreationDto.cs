﻿using System.ComponentModel.DataAnnotations;

namespace Entities.DataTransferObjects
{
    public class FleetUserCreationDto
    {
        [Required(ErrorMessage = "FleetId is required")]
        public long FleetId { get; set; }

        [Required(ErrorMessage = "Fleet is required")]
        [StringLength(60, ErrorMessage = "Fleet can't be longer than 60 characters")]
        public string Fleet { get; set; }

        [Required(ErrorMessage = "Username is required")]
        [StringLength(60, ErrorMessage = "Username can't be longer than 60 characters")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [StringLength(60, ErrorMessage = "Password can't be longer than 60 characters")]
        public string Password { get; set; }
    }
}
