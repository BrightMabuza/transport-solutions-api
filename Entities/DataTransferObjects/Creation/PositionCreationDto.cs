﻿using System.ComponentModel.DataAnnotations;

namespace Entities.DataTransferObjects.Creation
{
    public class PositionCreationDto
    {
        public float Long { get; set; }

        public bool LongSpecified { get; set; }

        public float Lat { get; set; }

        public bool LatSpecified { get; set; }

        public string PosText { get; set; }

        public string Course { get; set; }

        public string Speed { get; set; }

        public string KM { get; set; }

        public string GpsStatus { get; set; }
    }
}
