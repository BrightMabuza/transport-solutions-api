﻿namespace Entities.DataTransferObjects.Creation
{
    public class VehicleInfoCreationDto
    {
        public long vehicleId { get; set; }

        public string IgnitionStatus { get; set; }

        public bool IgnitionStatusSpecified { get; set; }

        public string MotorStatus { get; set; }

        public bool MotorStatusSpecified { get; set; }

        public string TrailerName { get; set; }
    }
}
