﻿namespace Entities.DataTransferObjects.Creation
{
    public class TripRecordCreationDto
    {
        public long TripId { get; set; }

        public string TripRecordKind { get; set; }

        public virtual VehicleInfoCreationDto VehicleInfo { get; set; }

        public long DriverNameId { get; set; }

        public bool DriverNameIdSpecified { get; set; }

        public virtual TripStartCreationDto Start { get; set; }

        public virtual TripEndCreationDto End { get; set; }

        public double Consumption { get; set; }

        public bool ConsumptionSpecified { get; set; }

        public double DiscChanges { get; set; }

        public bool DiscChangesSpecified { get; set; }

        public double TruckWeight { get; set; }

        public bool TruckWeightSpecified { get; set; }

        public double FuelLevel { get; set; }

        public bool FuelLevelSpecified { get; set; }

        public double AdBlueConsumption { get; set; }

        public bool AdBlueConsumptionSpecified { get; set; }

        public double AdBlueToFuelRatioConsumption { get; set; }

        public bool AdBlueToFuelRatioConsumptionSpecified { get; set; }

        public double AdBlueLevel { get; set; }

        public bool AdBlueLevelSpecified { get; set; }

        public override bool Equals(object obj)
        {
            if (!(obj is TripRecordCreationDto))
                return false;

            return true;
        }

    }
}
