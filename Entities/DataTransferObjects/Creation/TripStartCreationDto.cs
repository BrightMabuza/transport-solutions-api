﻿namespace Entities.DataTransferObjects.Creation
{
    public class TripStartCreationDto
    {
        public double Mileage { get; set; }

        public string VehicleTimeStamp { get; set; }

        public virtual PositionCreationDto Position { get; set; }
    }
}
