﻿namespace Entities.DataTransferObjects.Creation
{
    public class TripEndCreationDto
    {
        public double Mileage { get; set; }

        public string VehicleTimeStamp { get; set; }

        public virtual PositionCreationDto Position { get; set; }
    }
}
