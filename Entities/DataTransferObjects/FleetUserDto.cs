﻿using System;
namespace Entities.DataTransferObjects
{
    public class FleetUserDto
    {
        public Guid Id { get; set; }

        public long FleetId { get; set; }

        public string Fleet { get; set; }

        public string Username { get; set; }
    }
}
