﻿using System;

namespace Entities.DataTransferObjects
{
    public class VehiclePropertiesDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }
    }
}
