﻿namespace Entities.DataTransferObjects
{
    public class VehiclePropertiesCreationDto
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}
