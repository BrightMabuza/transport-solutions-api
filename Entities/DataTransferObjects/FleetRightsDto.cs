﻿using System;

namespace Entities.DataTransferObjects
{
    public class FleetRightsDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public string Service { get; set; }
    }
}
