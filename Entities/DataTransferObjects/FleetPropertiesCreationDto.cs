﻿using System.ComponentModel.DataAnnotations;

namespace Entities.DataTransferObjects
{
    public class FleetPropertiesCreationDto
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}