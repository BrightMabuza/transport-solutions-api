﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entities.DataTransferObjects
{
    public class FleetCreationDto
    {
        public long FleetId { get; set; }

        public string FleetName { get; set; }

        public string Street { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string CountryCode { get; set; }

        public string Region { get; set; }

        public string PageHeading { get; set; }

        public string FleetTimeZone { get; set; }

        public string FleetTimeZoneId { get; set; }

        public string DlSaving { get; set; }

        public string DeltaAmon { get; set; }

        public string DeltaAreaAmon { get; set; }

        public string InoId { get; set; }

        public virtual ICollection<FleetRightsCreationDto> FleetRights { get; set; }

        public virtual ICollection<FleetPropertiesCreationDto> FleetProperties { get; set; }

        public override bool Equals(object obj)
        {
            if (!(obj is FleetCreationDto))
                return false;

            return true;
        }

        public static bool operator ==(FleetCreationDto x, FleetCreationDto y) => x.Equals(y);

        public static bool operator !=(FleetCreationDto x, FleetCreationDto y) => !(x == y);
    }
}
