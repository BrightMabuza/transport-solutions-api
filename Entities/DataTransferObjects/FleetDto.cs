﻿using System;
using System.Collections.Generic;

namespace Entities.DataTransferObjects
{
    public class FleetDto
    {
        public Guid Id { get; set; }

        public long FleetId { get; set; }

        public string FleetName { get; set; }

        public string Street { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string CountryCode { get; set; }

        public string Region { get; set; }

        public string PageHeading { get; set; }

        public string FleetTimeZone { get; set; }

        public string FleetTimeZoneId { get; set; }

        public string DlSaving { get; set; }

        public string DeltaAmon { get; set; }

        public string DeltaAreaAmon { get; set; }

        public string InoId { get; set; }

        public virtual ICollection<FleetRightsDto> FleetRights { get; set; }

        public virtual ICollection<FleetPropertiesDto> FleetProperties { get; set; }
    }
}
