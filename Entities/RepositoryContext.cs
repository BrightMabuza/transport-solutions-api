﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;

namespace Entities
{
    public class RepositoryContext: DbContext
    {
        public RepositoryContext(DbContextOptions options): base(options)
        {
        }

        public DbSet<FleetUser> FleetUsers { get; set; }

        public DbSet<Fleet> Fleets { get; set; }

        public DbSet<Vehicle>  Vehicles { get; set; }

        public DbSet<VehicleGroup> VehicleGroups { get; set; }

        public DbSet<TripRecord> TripRecords { get; set; }
    }
}
