﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contracts;
using Entities;
using Entities.DataTransferObjects;
using Entities.Models;
using Fleetboard.Api.ApplicationLayer.ServiceFacade;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class VehicleServiceRepository : RepositoryBase<Vehicle>, IVehicleServiceRepository
    {
        IFleetServiceFacade serviceFacade;
        public VehicleServiceRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {
            serviceFacade = new FleetServiceFacade();
        }

        public List<VehicleCreationDto> GetVehicle() => serviceFacade.GetVehicles();

        public List<Vehicle> GetAllVehicles()
        {
            return FindAll().OrderBy(vehicle => vehicle.Chassis).ToList();
        }

        public Vehicle GetVehicleByVehicleId(long vehicleId)
        {
            return FindByCondition(vehicle => vehicle.INSMSISDN.Equals(vehicleId))
                .FirstOrDefault();
        }

        public List<Vehicle> GetVehicleByFleetId(long fleetId)
        {
            return FindByCondition(vehicle => vehicle.FleetId.Equals(fleetId)).ToList();
        }

        public Vehicle GetVehicleWithDetails(string chassis)
        {
            return FindByCondition(vehicle => vehicle.Chassis.Equals(chassis))
                .Include(vp => vp.VehicleProperties)
                .Include(vh => vh.VehicleHardware)
                .FirstOrDefault();
        }

        public void CreateVehicle(Vehicle vehicle)
        {
            Create(vehicle);
        }

        public void UpdateVehicle(Vehicle vehicle)
        {
            Update(vehicle);
        }

        public void DeleteVehicle(Vehicle vehicle)
        {
            Delete(vehicle);
        }
    }
}
