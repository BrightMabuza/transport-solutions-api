﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contracts;
using Entities;
using Entities.DataTransferObjects.Creation;
using Entities.Models;
using Fleetboard.Api.ApplicationLayer.ServiceFacade;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class TripRecordServiceRepository : RepositoryBase<TripRecord>, ITripRecordServiceRepository
    {
        ITripRecordServiceFacade serviceFacade;
        public TripRecordServiceRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {
            serviceFacade = new TripRecordServiceFacade();
        }

        public List<TripRecordCreationDto> GetTripRecord(long vehicleId, DateTime begin, DateTime end) => serviceFacade.GetTripRecords(vehicleId, begin, end);

        public TripRecord GetTripRecordById(long tripId)
        {
            return FindByCondition(tripRecord => tripRecord.TripId.Equals(tripId))
                .FirstOrDefault();
        }

        public TripRecord GetTripRecordWithDetails(long tripId)
        {
            return FindByCondition(tripRecord => tripRecord.TripId.Equals(tripId))
                .Include(v => v.VehicleInfo)
                .Include(s => s.Start)
                .ThenInclude(p => p.Position)
                .Include(e => e.End)
                .ThenInclude(p => p.Position)
                .FirstOrDefault();
        }

        public List<TripRecord> GetAllTripRecords()
        {
            return FindAll().OrderBy(tripRecord => tripRecord.TripRecordKind).ToList();
        }

        public void CreateTripRecord(TripRecord tripRecord)
        {
            Create(tripRecord);
        }

        public void UpdateTripRecord(TripRecord tripRecord)
        {
            Update(tripRecord);
        }

        public void DeleteTripRecord(TripRecord tripRecord)
        {
            Delete(tripRecord);
        }
    }
}
