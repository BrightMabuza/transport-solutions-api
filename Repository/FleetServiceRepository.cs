﻿using System.Collections.Generic;
using System.Linq;
using Contracts;
using Entities;
using Entities.DataTransferObjects;
using Entities.Models;
using Fleetboard.Api.ApplicationLayer.ServiceFacade;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class FleetServiceRepository : RepositoryBase<Fleet>, IFleetServiceRepository
    {
        IFleetServiceFacade serviceFacade;
        public FleetServiceRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {
            serviceFacade = new FleetServiceFacade();
        }

        public FleetCreationDto GetFleet(long fleetId) => serviceFacade.GetFleet(fleetId);

        public List<Fleet> GetAllFleets()
        {
            return FindAll().OrderBy(fleet => fleet.FleetName).ToList();
        }

        public Fleet GetFleetById(long fleetId)
        {
            return FindByCondition(fleet => fleet.FleetId.Equals(fleetId))
                .FirstOrDefault();
        }

        public Fleet GetFleetWithDetails(long fleetId)
        {
            return FindByCondition(fleet => fleet.FleetId.Equals(fleetId))
                .Include(fp => fp.FleetProperties)
                .Include(fr => fr.FleetRights)
                .FirstOrDefault();
        }

        public void CreateFleet(Fleet fleet)
        {
               Create(fleet);
        }

        public void UpdateFleet(Fleet fleet)
        {
            Update(fleet);
        }

        public void DeleteFleet(Fleet fleet)
        {
            Delete(fleet);
        }
    }
}
