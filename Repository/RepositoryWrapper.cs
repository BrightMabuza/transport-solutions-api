﻿using Contracts;
using Entities;

namespace Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private RepositoryContext _repositoryContext;
        private IFleetUserRepository _fleetuser;
        private IFleetServiceRepository _fleet;
        private IVehicleServiceRepository _vehicle;
        private IVehicleGroupRepository _vehicleGroup;
        private ITripRecordServiceRepository _trip;

        public RepositoryWrapper(RepositoryContext repositoryContext)
        {
            _repositoryContext = repositoryContext;
        }

        public IFleetUserRepository FleetUser {
            get
            {
                if(_fleetuser == null)
                {
                    _fleetuser = new FleetUserRepository(_repositoryContext);
                }

                return _fleetuser;
            }
        }

        public IFleetServiceRepository Fleet {
            get
            {
                if(_fleet == null)
                {
                    _fleet = new FleetServiceRepository(_repositoryContext);
                }

                return _fleet;
            }
        }

        public IVehicleServiceRepository Vehicle
        {
            get
            {
                if (_vehicle == null)
                {
                    _vehicle = new VehicleServiceRepository(_repositoryContext);
                }

                return _vehicle;
            }
        }

        public ITripRecordServiceRepository TripRecord
        {
            get
            {
                if(_trip == null)
                {
                    _trip = new TripRecordServiceRepository(_repositoryContext);
                }

                return _trip;
            }
        }

        public IVehicleGroupRepository VehicleGroup
        {
            get
            {
                if(_vehicleGroup == null)
                {
                    _vehicleGroup = new VehicleGroupRepository(_repositoryContext);
                }

                return _vehicleGroup;
            }
        }

        public void Save()
        {
            this._repositoryContext.SaveChanges();
        }
    }
}
