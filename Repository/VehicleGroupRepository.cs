﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contracts;
using Entities;
using Entities.DataTransferObjects.Creation;
using Entities.Models;
using Fleetboard.Api.ApplicationLayer.ServiceFacade;

namespace Repository
{
    public class VehicleGroupRepository : RepositoryBase<VehicleGroup>, IVehicleGroupRepository
    {
        IFleetServiceFacade serviceFacade;
        public VehicleGroupRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {
            serviceFacade = new FleetServiceFacade();
        }

        public IEnumerable<VehicleGroupCreationDto> GetVehicleGroups() => serviceFacade.GetVehicleGroups();

        public List<VehicleGroup> GetAllVehiclesGroups()
        {
            return FindAll().OrderBy(vehicleGroup => vehicleGroup.GroupName).ToList();
        }

        public void CreateVehicleGroup(VehicleGroup vehicleGroup)
        {
            Create(vehicleGroup);
        }

        public VehicleGroup GetVehicleGroupById(string groupId)
        {
            return FindByCondition(vehicleGroup => vehicleGroup.GroupId.Equals(groupId))
                .FirstOrDefault();
        }

        public void UpdateVehicleGroup(VehicleGroup vehicleGroup)
        {
            Update(vehicleGroup);
        }

        public void DeleteVehicleGroup(VehicleGroup vehicleGroup)
        {
            Delete(vehicleGroup);
        }
    }
}
