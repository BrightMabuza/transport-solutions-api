﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contracts;
using Entities;
using Entities.Models;
using Fleetboard.Api.ApplicationLayer.ServiceFacade;

namespace Repository
{
    public class FleetUserRepository : RepositoryBase<FleetUser>, IFleetUserRepository
    {
        IAuthServiceFacade authServiceFacade;
        public FleetUserRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {
            authServiceFacade = new AuthServiceFacade();
        }

        public IEnumerable<FleetUser> GetAllFleetUsers()
        {
            return FindAll()
                .OrderBy(fu => fu.Username)
                .ToList();
        }

        public FleetUser GetFleetUserById(long Id)
        {
            return FindByCondition(fleetUser => fleetUser.FleetId.Equals(Id))
                .FirstOrDefault();
        }

        public void PostFleetUser(FleetUser fleetUser)
        {
            fleetUser.Fleet = fleetUser.Fleet.ToUpper();

            try
            {
                authServiceFacade.Login(fleetUser.Fleet,fleetUser.Username, fleetUser.Password);

                fleetUser.FleetId = authServiceFacade.GetCurrentUser().FleetId;

                Create(fleetUser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateFleet(FleetUser fleetUser)
        {
            Update(fleetUser);
        }

        public void DeleteFleetUser(FleetUser fleetUser)
        {
            Delete(fleetUser);
        }
    }
}
