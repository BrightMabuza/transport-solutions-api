﻿using System;
using AutoMapper;
using Contracts;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Transport_Solutions_Api.Controllers
{
    [Route("api/[controller]")]
    public class FleetServiceController : Controller
    {
        private readonly IRepositoryWrapper _repositoryContext;
        private IFleetServiceFacade _fleetService;
        private IMapper _mapper;

        public FleetServiceController(IRepositoryWrapper repositoryWrapper, IFleetServiceFacade fleetService, IMapper mapper)
        {
            _repositoryContext = repositoryWrapper;
            _fleetService = fleetService;
            _mapper = mapper;
        }

        [HttpGet("GetFleet")]
        public IActionResult GetFleet(long fleetId)
        {
            try
            {
                return new ObjectResult(_fleetService.GetFleet(fleetId)) { StatusCode = 200 };
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
