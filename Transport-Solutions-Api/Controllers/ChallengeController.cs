﻿using System.Security.Cryptography;
using Microsoft.AspNetCore.Mvc;
using Transport_Solutions_Api.Helpers;
using Transport_Solutions_Api.Models;
using Transport_Solutions_Api.Models.Response;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Transport_Solutions_Api.Controllers
{
    [Route("api/[controller]")]
    public class ChallengeController : ControllerBase
    {
        private readonly ICrypto _crypto;

        public ChallengeController(ICrypto crypto)
        {
            _crypto = crypto;

        }
        // POST api/values
        [HttpPost]
        public ChallengeResponseModel Post([FromBody]ChallengeModel value)
        {
            byte[] challenge = _crypto.Base64UrlDecode(value.challenge);
            byte[] secret = _crypto.Base64UrlDecode(value.secretKey);
            byte[] hash = new HMACSHA256(secret).ComputeHash(challenge);

            return new ChallengeResponseModel { Key = _crypto.Base64UrlEncode(hash) };
        }
    }
}
