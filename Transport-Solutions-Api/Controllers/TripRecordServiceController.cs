﻿
using System;
using AutoMapper;
using Contracts;
using Microsoft.AspNetCore.Mvc;

namespace Transport_Solutions_Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TripRecordServiceController : ControllerBase
    {
        #region Fields

        private readonly IRepositoryWrapper _repositoryContext;
        private ITripRecordServiceFacade _tripRecordService;
        private IMapper _mapper;

        #endregion
        #region Constructor
        public TripRecordServiceController(IRepositoryWrapper repositoryContext, ITripRecordServiceFacade tripRecordService , IMapper mapper)
        {
            _repositoryContext = repositoryContext;
            _tripRecordService = tripRecordService;
            _mapper = mapper;
        }
        #endregion
        #region Methods

        [HttpPost("GetTripRecord")]
        public IActionResult GetTripRecord(long vehicleId, DateTime begin, DateTime end)
        {

            try
            {
                if (vehicleId == 0 || begin == null || end == null)
                    return BadRequest("Parameters cannot be empty");

                return new ObjectResult((_tripRecordService.GetTripRecords(vehicleId,begin, end))) { StatusCode = 200 };
            }
            catch (Exception ex)
            {
                return BadRequest(error: ex.Message);
            }
        }

        #endregion
    }
}