﻿using System;
using AutoMapper;
using Contracts;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Transport_Solutions_Api.Controllers
{
    [Route("api/[controller]")]
    public class VehicleServiceController : ControllerBase
    {
        private readonly IRepositoryWrapper _repositoryContext;
        private IFleetServiceFacade _fleetService;
        private IMapper _mapper;

        public VehicleServiceController(IRepositoryWrapper repositoryWrapper, IFleetServiceFacade fleetService, IMapper mapper)
        {
            _repositoryContext = repositoryWrapper;
            _fleetService = fleetService;
            _mapper = mapper;
        }

        [HttpGet("GetVehicleGroups")]
        public IActionResult GetVehicleGroups()
        {
            try
            {
                return new ObjectResult(_fleetService.GetVehicleGroups()) { StatusCode = 200 };
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"{ex.Message}");
            }
        }

        [HttpGet("GetVehicles")]
        public IActionResult GetVehicles()
        {
            try
            {
                return new ObjectResult(_fleetService.GetVehicles()) { StatusCode = 200 };
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"{ex.Message}");
            }
        }
    }
}
