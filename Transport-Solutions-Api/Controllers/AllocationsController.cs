﻿using System;
using System.Collections.Generic;
using System.Threading;
using Levelmaster;
using Microsoft.AspNetCore.Mvc;
using Transport_Solutions_Api.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Transport_Solutions_Api.Controllers
{
    [Route("api/[controller]")]
    public class AllocationsController : ControllerBase
    {
        private static LMTcpClient _lmTcpClient = null;
        private static Allocation _allocation = null;
        static List<Models.ClientModel> _clients = null;
        static List<Models.SiteModel> _sites = null;
        static List<Models.ContainerModel> _containers = null;
        private static CancellationTokenSource _cancelSource = null;

        public AllocationsController()
        {
            _cancelSource = new CancellationTokenSource();
            var cancelToken = _cancelSource.Token;

            _lmTcpClient = new LMTcpClient();

            _lmTcpClient.OnAllocationReceived += _lmTcpClient_OnAllocationReceived;
            _lmTcpClient.OnTCPConnected += _lmTcpClient_OnConnected;
            _lmTcpClient.OnTCPDisconnected += _lmTcpClient_OnDisconnected;
            _lmTcpClient.OnErrorOccured += _lmTcpClient_OnErrorOccured;
            _lmTcpClient.OnStreamConnected += _lmTcpClient_OnStreamConnected;
            _lmTcpClient.OnStreamDisconnected += _lmTcpClient_OnStreamDisconnected;
            _lmTcpClient.OnDataReceived += _lmTcpClient_OnDataReceived;

            _lmTcpClient.Connect("bright.mabuza@inteleqt.co.za", "P@ssword$$01", cancelToken);
            _clients = new List<Models.ClientModel>();
            _containers = new List<Models.ContainerModel>();
            _sites = new List<Models.SiteModel>();
            if (_lmTcpClient.State == LMTcpState.CONNECTED)
            {
                foreach (var levelAllocation in _allocation.Values)
                {
                    var LevelClient = new Models.ClientModel
                    {
                        Name = levelAllocation.Name,
                        UUID = levelAllocation.UUID,
                    };

                    _clients.Add(LevelClient);

                    foreach (var levelSite in levelAllocation.Sites.Values)
                    {
                        var LevelSite = new Models.SiteModel
                        {
                            Name = levelSite.Name,
                            UUID = levelSite.UUID,
                            Active = levelSite.Active,
                            clientUUID = levelAllocation.UUID
                        };

                        _sites.Add(LevelSite);

                        foreach (var levelContainer in levelSite.Containers.Values)
                        {
                            var LevelContainer = new Models.ContainerModel
                            {
                                Name = levelContainer.Name,
                                ContainerUUID = levelContainer.ContainerUUID,
                                SiteUUID = levelContainer.SiteUUID,
                                ClientUUID = levelContainer.ClientUUID,
                                Content = levelContainer.Content,
                                Capacity = levelContainer.Capacity,
                                Temperature = levelContainer.Temperature,
                                Volume = levelContainer.Volume,
                                Water_Bottom = levelContainer.Water_Bottom,
                                Water_Top = levelContainer.Water_Top,
                                CalibrationFault = levelContainer.CalibrationFault,
                                Distance = levelContainer.Distance,
                                DiffUpdate = levelContainer.DiffUpdate,
                                LastUpdate = levelContainer.LastUpdate,
                                Reading = levelContainer.Reading
                            };

                            _containers.Add(LevelContainer);
                        }
                    }
                }
            }
        }
        // POST api/values
        [HttpGet("clients")]
        public List<ClientModel> Get()
        {

            List<Models.ClientModel> getClients = new List<Models.ClientModel>();
            _clients.ForEach(c =>
            {
                c.Sites = new List<Models.SiteModel>();
                _sites.ForEach(s =>
                {
                    if (c.UUID == s.clientUUID)
                    {
                        c.Sites.Add(s);

                        s.Containers = new List<Models.ContainerModel>();
                        _containers.ForEach(co => {
                            if (co.SiteUUID == s.UUID)
                            {

                                s.Containers.Add(co);
                            }
                        });
                    }
                });

                getClients.Add(c);
            });
            return getClients;
        }

        // GET: api/allocations/sites
        [HttpGet("sites")]
        public List<Models.SiteModel> sites()
        {
            List<Models.SiteModel> getSites = new List<Models.SiteModel>();
            _sites.ForEach(s => {
                s.Containers = new List<Models.ContainerModel>();
                _containers.ForEach(c => {
                    if (c.SiteUUID == s.UUID)
                        s.Containers.Add(c);
                });

                getSites.Add(s);
            });
            return _sites;
        }

        // GET: api/allocations/containers
        [HttpGet("containers")]
        public List<Models.ContainerModel> containers()
        {
            return _containers;
        }

        private static void _lmTcpClient_OnStreamErrorOccured(object sender, LMErrorType errorType, string message)
        {
            Console.WriteLine($"Stream Error Occured {message}");
        }

        private static void _lmTcpClient_OnStreamDisconnected(object sender, string message)
        {
            Console.WriteLine("Stream Disconnected");
        }

        private static void _lmTcpClient_OnStreamConnected(object sender, string message)
        {
            Console.WriteLine("Stream Connected");
        }

        private static void _lmTcpClient_OnDataReceived(Container container)
        {
            // TODO: Ingest this data
            Console.WriteLine($"Data Received: {container.Name} {container.Volume} Liters");
        }

        private static void _lmTcpClient_OnErrorOccured(object sender, LMErrorType errorType, string message)
        {
            Console.WriteLine($"Error Occured [Sender:{sender}] [Message: {message}]");
        }

        private static void _lmTcpClient_OnDisconnected(object sender, string message)
        {
            Console.WriteLine("Disconnected");
        }

        private static void _lmTcpClient_OnConnected(object sender, string message)
        {
            Console.WriteLine("Connected");
        }

        private static void _lmTcpClient_OnAllocationReceived(Allocation allocation)
        {
            _allocation = allocation;
            Console.WriteLine("Allocation Received");

        }
    }
}
