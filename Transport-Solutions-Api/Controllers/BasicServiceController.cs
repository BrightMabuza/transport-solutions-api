﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Contracts;
using Entities.DataTransferObjects;
using Entities.Models;
using Fleetboard.Api.ApplicationLayer.ServiceFacade;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Transport_Solutions_Api.Controllers
{
    [Route("api/[controller]")]
    public class BasicServiceController : ControllerBase
    {
        private readonly IRepositoryWrapper _repositoryContext;
        private readonly IAuthServiceFacade _authService;
        private IMapper _mapper;
        private readonly ILoggerManager _logger;

        public BasicServiceController(IRepositoryWrapper repositoryWrapper, IMapper mapper,ILoggerManager logger)
        {
            _repositoryContext = repositoryWrapper;
            _authService = new AuthServiceFacade();
            _mapper = mapper;
            _logger = logger;
        }

        [HttpPost("CreateFleetUser")]
        public IActionResult Post([FromBody]FleetUserCreationDto fleetUser)
        {
            _logger.LogInfo(fleetUser.Username);
            try
            {
                if (fleetUser == null)
                {
                    return BadRequest("FleetUser model is null");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid Model Object!");
                }

                _authService.Login(fleetUser.Fleet, fleetUser.Username, fleetUser.Password);

                fleetUser.FleetId = _authService.GetCurrentUser().FleetId;

                var fleetUserEntity = _mapper.Map<FleetUser>(fleetUser);

                _repositoryContext.FleetUser.PostFleetUser(fleetUserEntity);
                _repositoryContext.Save();

                var createdFleetUser = _mapper.Map<FleetUserDto>(fleetUserEntity);

                return new ObjectResult(createdFleetUser) { StatusCode = 200 };
            }
            catch (Exception ex)
            {
                return StatusCode(401, ex.Message);
            }
        }

        [HttpPost("Login")]
        public IActionResult Login([FromBody]FleetUserCreationDto fleetUser)
        {
            try
            {
                if (fleetUser == null)
                {
                    return BadRequest("FleetUser model is null");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid Model Object!");
                }

                var sessionId = _authService.Login(fleetUser.Fleet, fleetUser.Username, fleetUser.Password);


                return new ObjectResult(sessionId) { StatusCode = 200 };
            }
            catch (Exception ex)
            {
                return StatusCode(401, ex.Message);
            }
        }

        [HttpGet("GetAllFleetUser")]
        public IActionResult GetAllFleetUsers()
        {
            try
            {
                var users = _repositoryContext.FleetUser.GetAllFleetUsers();

                var usersResults = _mapper.Map<IEnumerable<FleetUserDto>>(users);

                return Ok(usersResults);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("GetFleetUserById")]
        public IActionResult GetFleetUserById(long fleetId)
        {
            try
            {
                var user = _repositoryContext.FleetUser.GetFleetUserById(fleetId);

                var usersResults = _mapper.Map<IEnumerable<FleetUserDto>>(user);

                return Ok(usersResults);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPut("UpdateFleetUser")]
        public IActionResult UpdateFleetUser([FromBody]FleetUserCreationDto fleetUser)
        {
            try
            {
                if (fleetUser == null)
                {
                    return BadRequest("Owner object is null");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }

                var FleetUserEntity = _repositoryContext.FleetUser.GetFleetUserById(fleetUser.FleetId);

                if (FleetUserEntity == null)
                {
                    return NotFound();
                }

                _mapper.Map(fleetUser, FleetUserEntity);

                _repositoryContext.FleetUser.UpdateFleet(FleetUserEntity);
                _repositoryContext.Save();

                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpDelete("DeleteFleetuser")]
        public IActionResult DeleteOwner(long fleetId)
        {
            try
            {
                var fleetUser = _repositoryContext.FleetUser.GetFleetUserById(fleetId);
                if (fleetUser == null)
                {
                    return NotFound();
                }

                _repositoryContext.FleetUser.DeleteFleetUser(fleetUser);
                _repositoryContext.Save();

                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
