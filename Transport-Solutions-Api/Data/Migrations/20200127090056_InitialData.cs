﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Transport_Solutions_Api.Data.Migrations
{
    public partial class InitialData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Fleets",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FleetId = table.Column<long>(nullable: false),
                    FleetName = table.Column<string>(nullable: true),
                    Street = table.Column<string>(nullable: true),
                    PostalCode = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    CountryCode = table.Column<string>(nullable: true),
                    Region = table.Column<string>(nullable: true),
                    PageHeading = table.Column<string>(nullable: true),
                    FleetTimeZone = table.Column<string>(nullable: true),
                    FleetTimeZoneId = table.Column<string>(nullable: true),
                    DlSaving = table.Column<string>(nullable: true),
                    DeltaAmon = table.Column<string>(nullable: true),
                    DeltaAreaAmon = table.Column<string>(nullable: true),
                    InoId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fleets", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "fleetUser",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FleetId = table.Column<long>(nullable: false),
                    Fleet = table.Column<string>(maxLength: 60, nullable: false),
                    Username = table.Column<string>(maxLength: 60, nullable: false),
                    Password = table.Column<string>(maxLength: 60, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_fleetUser", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Position",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Long = table.Column<float>(nullable: false),
                    LongSpecified = table.Column<bool>(nullable: false),
                    Lat = table.Column<float>(nullable: false),
                    LatSpecified = table.Column<bool>(nullable: false),
                    PosText = table.Column<string>(nullable: true),
                    Course = table.Column<string>(nullable: true),
                    Speed = table.Column<string>(nullable: true),
                    KM = table.Column<string>(nullable: true),
                    GpsStatus = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Position", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VehicleGroups",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    GroupId = table.Column<string>(nullable: true),
                    FleetId = table.Column<long>(nullable: false),
                    GroupName = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    InoId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleGroups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VehicleInfo",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    vehicleId = table.Column<long>(nullable: false),
                    IgnitionStatus = table.Column<string>(nullable: true),
                    IgnitionStatusSpecified = table.Column<bool>(nullable: false),
                    MotorStatus = table.Column<string>(nullable: true),
                    MotorStatusSpecified = table.Column<bool>(nullable: false),
                    TrailerName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleInfo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Vehicles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Chassis = table.Column<string>(nullable: true),
                    FleetId = table.Column<long>(nullable: false),
                    INSMSISDN = table.Column<string>(nullable: true),
                    Registration = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    VehicleNumber = table.Column<string>(nullable: true),
                    VehicleType = table.Column<string>(nullable: true),
                    EndTime = table.Column<string>(nullable: true),
                    StartTime = table.Column<string>(nullable: true),
                    RegistrationDate = table.Column<string>(nullable: true),
                    TelematicGroup = table.Column<int>(nullable: false),
                    TelematicGroupSpecified = table.Column<bool>(nullable: false),
                    InoId = table.Column<string>(nullable: true),
                    VehicleGroupId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vehicles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FleetProperties",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true),
                    FleetId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FleetProperties", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FleetProperties_Fleets_FleetId",
                        column: x => x.FleetId,
                        principalTable: "Fleets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FleetRights",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    Service = table.Column<string>(nullable: true),
                    FleetId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FleetRights", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FleetRights_Fleets_FleetId",
                        column: x => x.FleetId,
                        principalTable: "Fleets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TripEnd",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Mileage = table.Column<double>(nullable: false),
                    VehicleTimeStamp = table.Column<string>(nullable: true),
                    PositionId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TripEnd", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TripEnd_Position_PositionId",
                        column: x => x.PositionId,
                        principalTable: "Position",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TripStart",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Mileage = table.Column<double>(nullable: false),
                    VehicleTimeStamp = table.Column<string>(nullable: true),
                    PositionId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TripStart", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TripStart_Position_PositionId",
                        column: x => x.PositionId,
                        principalTable: "Position",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VehicleHardware",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    HardwareId = table.Column<string>(nullable: true),
                    Hardware = table.Column<string>(nullable: true),
                    VehicleId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleHardware", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleHardware_Vehicles_VehicleId",
                        column: x => x.VehicleId,
                        principalTable: "Vehicles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VehicleProperties",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true),
                    VehicleId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleProperties", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleProperties_Vehicles_VehicleId",
                        column: x => x.VehicleId,
                        principalTable: "Vehicles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TripRecords",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TripId = table.Column<long>(nullable: false),
                    TripRecordKind = table.Column<string>(nullable: true),
                    VehicleInfoId = table.Column<Guid>(nullable: true),
                    DriverNameId = table.Column<long>(nullable: false),
                    DriverNameIdSpecified = table.Column<bool>(nullable: false),
                    StartId = table.Column<Guid>(nullable: true),
                    EndId = table.Column<Guid>(nullable: true),
                    Consumption = table.Column<double>(nullable: false),
                    ConsumptionSpecified = table.Column<bool>(nullable: false),
                    DiscChanges = table.Column<double>(nullable: false),
                    DiscChangesSpecified = table.Column<bool>(nullable: false),
                    TruckWeight = table.Column<double>(nullable: false),
                    TruckWeightSpecified = table.Column<bool>(nullable: false),
                    FuelLevel = table.Column<double>(nullable: false),
                    FuelLevelSpecified = table.Column<bool>(nullable: false),
                    AdBlueConsumption = table.Column<double>(nullable: false),
                    AdBlueConsumptionSpecified = table.Column<bool>(nullable: false),
                    AdBlueToFuelRatioConsumption = table.Column<double>(nullable: false),
                    AdBlueToFuelRatioConsumptionSpecified = table.Column<bool>(nullable: false),
                    AdBlueLevel = table.Column<double>(nullable: false),
                    AdBlueLevelSpecified = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TripRecords", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TripRecords_TripEnd_EndId",
                        column: x => x.EndId,
                        principalTable: "TripEnd",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TripRecords_TripStart_StartId",
                        column: x => x.StartId,
                        principalTable: "TripStart",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TripRecords_VehicleInfo_VehicleInfoId",
                        column: x => x.VehicleInfoId,
                        principalTable: "VehicleInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FleetProperties_FleetId",
                table: "FleetProperties",
                column: "FleetId");

            migrationBuilder.CreateIndex(
                name: "IX_FleetRights_FleetId",
                table: "FleetRights",
                column: "FleetId");

            migrationBuilder.CreateIndex(
                name: "IX_TripEnd_PositionId",
                table: "TripEnd",
                column: "PositionId");

            migrationBuilder.CreateIndex(
                name: "IX_TripRecords_EndId",
                table: "TripRecords",
                column: "EndId");

            migrationBuilder.CreateIndex(
                name: "IX_TripRecords_StartId",
                table: "TripRecords",
                column: "StartId");

            migrationBuilder.CreateIndex(
                name: "IX_TripRecords_VehicleInfoId",
                table: "TripRecords",
                column: "VehicleInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_TripStart_PositionId",
                table: "TripStart",
                column: "PositionId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleHardware_VehicleId",
                table: "VehicleHardware",
                column: "VehicleId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleProperties_VehicleId",
                table: "VehicleProperties",
                column: "VehicleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FleetProperties");

            migrationBuilder.DropTable(
                name: "FleetRights");

            migrationBuilder.DropTable(
                name: "fleetUser");

            migrationBuilder.DropTable(
                name: "TripRecords");

            migrationBuilder.DropTable(
                name: "VehicleGroups");

            migrationBuilder.DropTable(
                name: "VehicleHardware");

            migrationBuilder.DropTable(
                name: "VehicleProperties");

            migrationBuilder.DropTable(
                name: "Fleets");

            migrationBuilder.DropTable(
                name: "TripEnd");

            migrationBuilder.DropTable(
                name: "TripStart");

            migrationBuilder.DropTable(
                name: "VehicleInfo");

            migrationBuilder.DropTable(
                name: "Vehicles");

            migrationBuilder.DropTable(
                name: "Position");
        }
    }
}
