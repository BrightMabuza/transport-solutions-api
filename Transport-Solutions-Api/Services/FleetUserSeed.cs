﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Contracts;
using Entities.DataTransferObjects;
using Entities.Models;
using Fleetboard.Api.ApplicationLayer.ServiceFacade;
using Transport_Solutions_Api.Helpers;

namespace Transport_Solutions_Api.Services
{
    public class FleetUserSeed
    {
        private readonly IRepositoryWrapper _repositoryContext;
        private readonly IAuthServiceFacade _authService;
        private IMapper _mapper;
        private ICrypto _crypto;
        private readonly ILoggerManager _logger;

        public FleetUserSeed(IRepositoryWrapper repositoryContext, IMapper mapper, ICrypto crypto,ILoggerManager logger)
        {
            _repositoryContext = repositoryContext;
            _authService = new AuthServiceFacade();
            _mapper = mapper;
            _crypto = crypto;
            _logger = logger;
        }

        public void Initialize()
        {
            if (_repositoryContext.FleetUser.GetAllFleetUsers().ToList().Count != 0)
                return;

            GetFleetUsers()
                .ForEach(fleetUser =>
                {
                    _authService.Login(fleetUser.Fleet, fleetUser.Username, fleetUser.Password);

                    fleetUser.FleetId = _authService.GetCurrentUser().FleetId;
                    fleetUser.Password = _crypto.Encryptword(fleetUser.Password);

                    var fleetUserEntity = _mapper.Map<FleetUser>(fleetUser);

                    _repositoryContext.FleetUser.Create(fleetUserEntity);
                    _logger.LogInfo($"{fleetUser.Username} has been added to the DB.");
                    _authService.Logout();
                });
            _repositoryContext.Save();
        }

        public static List<FleetUserCreationDto> GetFleetUsers()
        {
            List<FleetUserCreationDto> FleetUser = new List<FleetUserCreationDto>() {
                new FleetUserCreationDto { Fleet="BAKERS",Username="Inteleqt", Password="Password01" },
                new FleetUserCreationDto {Fleet="FPDUTOIT", Username="Inteleqt", Password="Password01" },
                new FleetUserCreationDto {Fleet="FAITH WHEELS TANKERS", Username="Inteleqt", Password="Password01" },
                new FleetUserCreationDto {Fleet="LOUTRANS", Username="Inteleqt", Password="Password01" }
        };
            return FleetUser;
        }

    }
}
