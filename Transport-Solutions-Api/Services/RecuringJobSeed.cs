﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AutoMapper;
using Contracts;
using Entities.DataTransferObjects;
using Entities.DataTransferObjects.Creation;
using Entities.Models;
using Fleetboard.Api.ApplicationLayer.ServiceFacade;
using Hangfire;
using Transport_Solutions_Api.Helpers;

namespace Transport_Solutions_Api.Services
{
    public class RecuringJobSeed
    {
        private readonly IRepositoryWrapper _repositoryContext;
        private IFleetServiceFacade _serviceFacade;
        private readonly IAuthServiceFacade _authService;
        private ITripRecordServiceFacade _tripRecordService;
        private readonly IMapper _mapper;
        private ICrypto _crypto;
        private readonly ILoggerManager _logger;

        public RecuringJobSeed(IMapper mapper, IRepositoryWrapper repositoryWrapper, ICrypto crypto, ILoggerManager logger)
        {
            _mapper = mapper;
            _repositoryContext = repositoryWrapper;
            _authService = new AuthServiceFacade();
            _crypto = crypto;
            _logger = logger;
        }

        public void DataSeed(DateTime beginDate, DateTime endDate)
        {
            Stopwatch stopWatch = new Stopwatch();
            try
            {
                var users = _repositoryContext.FleetUser.GetAllFleetUsers();

                var date = DateTime.Now;
                stopWatch.Start();
                users.ToList().ForEach(user => {
                    //TODO: log each user
                    _authService.Login(user.Fleet, user.Username, _crypto.Decryptword(user.Password));

                    _serviceFacade = new FleetServiceFacade();
                    _tripRecordService = new TripRecordServiceFacade();

                    FleetDataImporter(user.FleetId);
                    VehicleGroupDataImporter();

                    VehiclesDataImporter()
                    .ForEach(vehicle =>
                    {
                        TripRecordDataImporter(vehicle.InoId, beginDate, endDate);
                    });
                });
                stopWatch.Stop();
                TimeSpan ts = stopWatch.Elapsed;

                // Format and display the TimeSpan value. 
                string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                    ts.Hours, ts.Minutes, ts.Seconds,
                    ts.Milliseconds / 10);
                Console.WriteLine("RunTime " + elapsedTime);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void FleetDataImporter(long fleetId)
        {
            try
            {
                FleetCreationDto resDtoFleet = _serviceFacade.GetFleet(fleetId);
                Fleet fleetEntity = new Fleet();

                var fleet = _repositoryContext.Fleet.GetFleetWithDetails(resDtoFleet.FleetId);
                var fleetCreation = _mapper.Map<FleetCreationDto>(fleet);
                if (fleet == null)
                {
                    fleetEntity = _mapper.Map<Fleet>(resDtoFleet);
                    _repositoryContext.Fleet.CreateFleet(fleetEntity);
                    _logger.LogInfo($"Fleet : {fleetEntity.FleetName } has been saved on the Database");
                    
                }
                _repositoryContext.Save();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Fleet Error: {ex.Message}");
            }
        }

        private void VehicleGroupDataImporter()
        {
            try
            {
                var dtoVehicleGroup = _serviceFacade.GetVehicleGroups();
                VehicleGroup vehicleGroupEntity = new VehicleGroup();

                dtoVehicleGroup.ToList()
                    .ForEach(resVehicleGroup => {
                    var vehicleGroup = _repositoryContext.VehicleGroup.GetVehicleGroupById(resVehicleGroup.GroupId);
                    var vehicleGroupCreationDto = _mapper.Map<VehicleGroupCreationDto>(vehicleGroup);

                    if (vehicleGroup == null)
                    {
                        vehicleGroupEntity = _mapper.Map<VehicleGroup>(resVehicleGroup);
                        _repositoryContext.VehicleGroup.CreateVehicleGroup(vehicleGroupEntity);
                            _logger.LogInfo($"VehicleGroup: {vehicleGroupEntity.GroupName} has been saved on the DB");
                    }
                });
                _repositoryContext.Save();

            }
            catch (Exception ex)
            {
                _logger.LogError($"VehicleGroup Error: {ex.Message}");
            }
        }

        private List<VehicleCreationDto> VehiclesDataImporter()
        {
            try
            {
                var dtoVehicle = _serviceFacade.GetVehicles();
                Vehicle vehicleEntity = new Vehicle();

                dtoVehicle.ForEach(resDtoVehicle => {
                    var vehicle = _repositoryContext.Vehicle.GetVehicleByVehicleId(long.Parse(resDtoVehicle.InoId));
                    var vehicleCreation = _mapper.Map<VehicleCreationDto>(vehicle);

                    if (vehicle == null)
                    {
                        vehicleEntity = _mapper.Map<Vehicle>(resDtoVehicle);
                        _repositoryContext.Vehicle.CreateVehicle(vehicleEntity);
                        _logger.LogInfo($"Vehicle: {vehicleEntity.InoId} has been save on the DB");
                    }

                });
                _repositoryContext.Save();

                return dtoVehicle;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Vehicle Error: {ex.Message}");
                return null;
            }
        }

        private void TripRecordDataImporter(string vehicleId, DateTime begin, DateTime end)
        {
            try
            {
                var dtoTrips = _tripRecordService.GetTripRecords(long.Parse(vehicleId), begin, end);
                TripRecord tripEntity = new TripRecord();

                dtoTrips.ForEach(resTripRecord =>
                {
                    var trip = _repositoryContext.TripRecord.GetTripRecordById(resTripRecord.TripId);
                    var tripCreation = _mapper.Map<TripRecordCreationDto>(resTripRecord);

                    if (trip == null)
                    {
                        tripEntity = _mapper.Map<TripRecord>(resTripRecord);
                        _repositoryContext.TripRecord.CreateTripRecord(tripEntity);
                        _logger.LogInfo($"TripRecord: {tripEntity.TripId} has been added to the DB.");
                    }
                });
                _repositoryContext.Save();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Trip Record: {ex.Message}");
            }
        }
    }
}
