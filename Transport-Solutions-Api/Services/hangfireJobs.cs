﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Contracts;
using Entities.DataTransferObjects;
using Entities.DataTransferObjects.Creation;
using Entities.Models;
using Fleetboard.Api.ApplicationLayer.ServiceFacade;
using Hangfire;
using Transport_Solutions_Api.Helpers;

namespace Transport_Solutions_Api.Services
{
    public class hangfireJobs
    {
        private readonly IRepositoryWrapper _repositoryContext;
        private IFleetServiceFacade _serviceFacade;
        private readonly IAuthServiceFacade _authService;
        private ITripRecordServiceFacade _tripRecordService;
        private readonly IMapper _mapper;
        private ICrypto _crypto;

        public hangfireJobs(IMapper mapper, IRepositoryWrapper repositoryWrapper,ICrypto crypto)
        {
            _mapper = mapper;
            _repositoryContext = repositoryWrapper;
            _authService = new AuthServiceFacade();
            _crypto = crypto;
        }
        public void RunAsync(DateTime begin, DateTime end)
        {
            InitialDataSeed(begin, end);

            //RecurringJob.AddOrUpdate(() => ProcessFleetBoardData(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 00, 00, 00), DateTime.Now),Cron.Hourly(8));
        }

        private void InitialDataSeed(DateTime beginDate, DateTime endDate)
        {
            try
            {
                var users = _repositoryContext.FleetUser.GetAllFleetUsers();

                users.ToList().ForEach(user => {
                    //TODO: log each user
                    _authService.Login(user.Fleet, user.Username, _crypto.Decryptword(user.Password));

                    _serviceFacade = new FleetServiceFacade();
                    _tripRecordService = new TripRecordServiceFacade();

                    FleetDataImporter(user.FleetId)
                    .ForEach(fleet =>
                    {
                        VehiclesDataImporter()
                        .ForEach(vehicle =>
                        {
                            TripRecordDataImporter(vehicle.InoId, beginDate, endDate);
                        });
                    });
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private List<Fleet> FleetDataImporter(long fleetId)
        {
            try
            {
                FleetCreationDto resDtoFleet = _serviceFacade.GetFleet(fleetId);
             
                Fleet fleetEntity = _mapper.Map<Fleet>(resDtoFleet);
                _repositoryContext.Fleet.CreateFleet(fleetEntity);
                //TODO: log that a new fleet has been added to the database

                _repositoryContext.Save();

                return _repositoryContext.Fleet.GetAllFleets();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        private void vehicleGroupImporter()
        {
            try
            {
                var dtoVehicleGroup = _serviceFacade.GetVehicleGroups();

                dtoVehicleGroup.ToList().ForEach(resVehicleGroup => {

               VehicleGroup vehicleGroupEntity = _mapper.Map<VehicleGroup>(resVehicleGroup);
                _repositoryContext.VehicleGroup.CreateVehicleGroup(vehicleGroupEntity);

                });
                _repositoryContext.Save();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
 
        private List<Vehicle> VehiclesDataImporter()
        {
            try
            {
                var dtoVehicle = _serviceFacade.GetVehicles();

                dtoVehicle.ForEach(resDtoVehicle => {

               Vehicle vehicleEntity = _mapper.Map<Vehicle>(resDtoVehicle);
                _repositoryContext.Vehicle.CreateVehicle(vehicleEntity);
                //TODO: log that a new vehicle has been added to the database

                });
                _repositoryContext.Save();

                return _repositoryContext.Vehicle.GetAllVehicles();
            }
            catch (Exception ex)
            {
                //TODO: logger all the errors for this method.
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        private void TripRecordDataImporter(string vehicleId, DateTime begin, DateTime end)
        {
            try
            {
                var dtoTrips = _tripRecordService.GetTripRecords(long.Parse(vehicleId), begin, end);

                dtoTrips.ForEach(resTripRecord =>
                {
                    TripRecord tripEntity = _mapper.Map<TripRecord>(resTripRecord);
                    _repositoryContext.TripRecord.CreateTripRecord(tripEntity);
                    //TODO: log that a new Trip has been added to the database
                });
                _repositoryContext.Save();
            }
            catch (Exception ex)
            {
                //TODO: log all errors from the trip data context;
                Console.WriteLine(ex.Message);
            }
        }
    }
}
