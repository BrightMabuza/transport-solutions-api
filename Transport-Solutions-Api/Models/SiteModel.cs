﻿using System;
using System.Collections.Generic;

namespace Transport_Solutions_Api.Models
{
    public class SiteModel
    {
        public Guid UUID { get; set; }

        public string Name { get; set; }

        public bool Active { get; set; }

        public Guid clientUUID { get; set; }

        public List<ContainerModel> Containers { get; set; }
    }
}
