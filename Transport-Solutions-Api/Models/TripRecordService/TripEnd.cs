﻿using System.ComponentModel.DataAnnotations;

namespace Transport_Solutions_Api.Models.TripRecordService
{
    public class TripEnd
    {
        [Key]
        public int Id { get; set; }

        public double Mileage { get; set; }

        public string VehicleTimeStamp { get; set; }

        public virtual Position Position { get; set; }
    }
}
