﻿using System.ComponentModel.DataAnnotations;
using TripRecordServiceReference;

namespace Transport_Solutions_Api.Models.TripRecordService
{
    public class TripRecord
    {
        [Key]
        public long Id { get; set; }

        public GetTripRecordResponseTypeTripRecordReportTripRecordKind TripRecordKind { get; set; }

        public virtual VehicleInfo VehicleInfo { get; set; }

        public long DriverNameId { get; set; }

        public bool DriverNameIdSpecified { get; set; }

        public virtual TripStart Start { get; set; }

        public virtual TripEnd End { get; set; }

        public double Consumption { get; set; }

        public bool ConsumptionSpecified { get; set; }

        public double DiscChanges { get; set; }

        public bool DiscChangesSpecified { get; set; }

        public double TruckWeight { get; set; }

        public bool TruckWeightSpecified { get; set; }

        public double FuelLevel { get; set; }

        public bool FuelLevelSpecified { get; set; }

        public double AdBlueConsumption { get; set; }

        public bool AdBlueConsumptionSpecified { get; set; }

        public double AdBlueToFuelRatioConsumption { get; set; }

        public bool AdBlueToFuelRatioConsumptionSpecified { get; set; }

        public double AdBlueLevel { get; set; }

        public bool AdBlueLevelSpecified { get; set; }
    }
}
