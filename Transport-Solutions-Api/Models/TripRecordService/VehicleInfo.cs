﻿using System.ComponentModel.DataAnnotations;
using TripRecordServiceReference;

namespace Transport_Solutions_Api.Models.TripRecordService
{
    public class VehicleInfo
    {
        [Key]
        public long Id { get; set; }

        public ignitionStatusType IgnitionStatus { get; set; }

        public bool IgnitionStatusSpecified { get; set; }

        public motorStatusType MotorStatus { get; set; }

        public bool MotorStatusSpecified { get; set; }

        public string TrailerName { get; set; }
    }
}
