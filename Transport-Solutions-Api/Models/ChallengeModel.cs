﻿using System;
namespace Transport_Solutions_Api.Models
{
    public class ChallengeModel
    {
        public string challenge { get; set; }

        public string secretKey { get; set; }
    }
}
