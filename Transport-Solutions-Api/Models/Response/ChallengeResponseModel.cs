﻿using System;
namespace Transport_Solutions_Api.Models.Response
{
    public class ChallengeResponseModel
    {
        public string Key { get; set; }
    }
}
