﻿using System;
namespace Transport_Solutions_Api.Models
{
    public class ContainerModel
    {
        public Guid ClientUUID
        {
            get;
            set;
        }

        public Guid SiteUUID
        {
            get;
            set;
        }

        public Guid ContainerUUID
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Content
        {
            get;
            set;
        }

        public int Volume
        {
            get;
            set;
        }

        public int Capacity
        {
            get;
            set;
        }

        public DateTime LastUpdate
        {
            get;
            set;
        }

        public TimeSpan DiffUpdate
        {
            get;
            set;
        }

        public bool Water_Top
        {
            get;
            set;
        }

        public bool Water_Bottom
        {
            get;
            set;
        }

        public int Distance
        {
            get;
            set;
        }

        public int Temperature
        {
            get;
            set;
        }

        public bool CalibrationFault
        {
            get;
            set;
        }

        public object Reading
        {
            get;
            set;
        }
    }
}
