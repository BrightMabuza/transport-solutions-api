﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Transport_Solutions_Api.Models.TripRecord
{
    public class FleetVehicle
    {
        [Key]
        public long vehicleId { get; set; }
    }
}
