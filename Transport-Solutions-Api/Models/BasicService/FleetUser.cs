﻿using System.ComponentModel.DataAnnotations;

namespace Transport_Solutions_Api.Models.BasicService
{
    public class FleetUser
    {
        [Key]
        public long FleetId { get; set; }

        public string Fleet { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}
