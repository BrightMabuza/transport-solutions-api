﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Transport_Solutions_Api.Models.BasicService
{
    public class DriverToken
    {
        [Key]
        public int Id { get; set; }

        public int DriverTokenKind { get; set; }

        public string Token { get; set; }
    }
}
