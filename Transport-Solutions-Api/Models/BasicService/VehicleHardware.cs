﻿using System.ComponentModel.DataAnnotations;

namespace Transport_Solutions_Api.Models.BasicService
{
    public class VehicleHardware
    {

        [Key]
        public int Id { get; set; }

        public string HardwareId { get; set; }

        public string Hardware { get; set; }
    }
}
