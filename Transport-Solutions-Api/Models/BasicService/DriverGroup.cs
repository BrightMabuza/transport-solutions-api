﻿using System.ComponentModel.DataAnnotations;

namespace Transport_Solutions_Api.Models.BasicService
{
    public class DriverGroup
    {
        [Key]
        public long DriverGroupId { get; set; }

        public long FleetId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
