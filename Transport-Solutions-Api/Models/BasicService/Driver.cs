﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Transport_Solutions_Api.Models.BasicService
{
    public class Driver
    {
        [Key]
        public long DriverNameID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Locale { get; set; }

        public string Comment { get; set; }

        public int Status { get; set; }

        public bool StatusSpecified { get; set; }

        public virtual ICollection<DriverToken> DriverToken { get; set; }

        public virtual ICollection<DriverGroup> DriverGroup { get; set; }
    }
}
