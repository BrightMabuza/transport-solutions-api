﻿using System.ComponentModel.DataAnnotations;

namespace Transport_Solutions_Api.Models.BasicService
{
    public class FleetProperties
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }
    }
}