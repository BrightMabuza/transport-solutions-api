﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BasicServiceReference;

namespace Transport_Solutions_Api.Models.BasicService
{
    public class Vehicle
    {
        [Key]
        public string Chassis { get; set; }

        public long FleetId { get; set; }

        public string INSMSISDN { get; set; }

        public string Registration { get; set; }

        public string Status { get; set; }

        public string VehicleNumber { get; set; }

        public string VehicleType { get; set; }

        public string EndTime { get; set; }

        public string StartTime { get; set; }

        public string RegistrationDate { get; set; }

        public telematicGroupIDType TelematicGroup { get; set; }

        public bool TelematicGroupSpecified { get; set; }

        public string InoId { get; set; }

        public virtual ICollection<VehicleHardware> VehicleHardware { get; set; }

        public virtual List<string> VehicleGroupId { get; set; }

        public virtual ICollection<VehicleProperties> VehicleProperties { get; set; }
    }
}
