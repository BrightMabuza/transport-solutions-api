﻿using System.ComponentModel.DataAnnotations;

namespace Transport_Solutions_Api.Models.BasicService
{
    public class VehicleGroup
    {
        [Key]
        public string GroupId { get; set; }

        public long FleetId { get; set; }

        public string GroupName { get; set; }

        public string Description { get; set; }

        public string InoId { get; set; }
    }
}
