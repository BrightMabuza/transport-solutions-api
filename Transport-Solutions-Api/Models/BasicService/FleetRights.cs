﻿using System.ComponentModel.DataAnnotations;

namespace Transport_Solutions_Api.Models.BasicService
{
    public class FleetRights
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public string Service { get; set; }
    }
}
