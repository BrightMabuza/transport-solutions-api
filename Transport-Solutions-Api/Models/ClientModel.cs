﻿using System;
using System.Collections.Generic;

namespace Transport_Solutions_Api.Models
{
    public class ClientModel
    {
        public Guid UUID { get; set; }

        public string Name { get; set; }

        public List<SiteModel> Sites { get; set; }
    }
}
