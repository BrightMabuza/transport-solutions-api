﻿using Contracts;
using Entities;
using LoggerService;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Repository;

namespace Transport_Solutions_Api.Extentions
{
    public static class ServiceExtentions
    {
        public static void ConfigureLoggerService(this IServiceCollection services)
        {
            services.AddSingleton<ILoggerManager, LoggerManager>();
            services.AddSingleton<IServiceLog, ServiceLog>();
        }

        public static void ConfigureMySqlContext(this IServiceCollection services, IConfiguration config)
        {
            var connectionString = config["connectionStrings:defaultConnection"];
            services.AddEntityFrameworkSqlServer()
                .AddDbContext<RepositoryContext>(options =>
                options.UseSqlServer(connectionString,optionbuilder =>
                optionbuilder.MigrationsAssembly("Transport-Solutions-Api")).UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking));
        }

        public static void ConfigureRepositoryWrapper(this IServiceCollection services)
        {
            services.AddScoped<IRepositoryWrapper, RepositoryWrapper>();
        }
    }
}
