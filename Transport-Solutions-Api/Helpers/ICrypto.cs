﻿namespace Transport_Solutions_Api.Helpers
{
   public interface ICrypto
    {
        string Base64UrlEncode(byte[] value);

        byte[] Base64UrlDecode(string value);

        string Encryptword(string Encryptval);

        string Decryptword(string DecryptText);

    }
}
