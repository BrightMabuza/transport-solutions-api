using System;
using System.IO;
using AutoMapper;
using Contracts;
using Hangfire;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using NLog;
using Transport_Solutions_Api.Extentions;
using Transport_Solutions_Api.Helpers;
using Transport_Solutions_Api.Services;

namespace Transport_Solutions_Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            LogManager.LoadConfiguration(String.Concat(Directory.GetCurrentDirectory(), "/nlog.config"));
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<ICrypto, Crypto>();

            services.ConfigureLoggerService();

            services.ConfigureMySqlContext(Configuration);

            services.ConfigureRepositoryWrapper();

            services.AddAutoMapper(typeof(Startup));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            //services.AddHangfire(x => x.UseSqlServerStorage(Configuration.GetConnectionString("defaultConnection")));
            //services.AddHangfireServer();

            services.AddSwaggerGen(x => {
                x.SwaggerDoc("v1", new OpenApiInfo { Title = "Transport Solutions API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        [Obsolete]
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IMapper mapper, IRepositoryWrapper repositoryWrapper, ICrypto crypto, ILoggerManager logger)
        {
            //app.UseHangfireDashboard();
            //app.UseHangfireServer();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            new FleetUserSeed(repositoryWrapper, mapper, crypto, logger).Initialize();

            //var FleetboardSeeder = new RecuringJobSeed(mapper, repositoryWrapper, crypto, logger);

            /*var jobId = BackgroundJob.Schedule(
                        () => FleetboardSeeder.DataSeed(new DateTime(2019, 01, 01, 00, 00, 00), DateTime.Now),
                        TimeSpan.FromMinutes(2));
                        */
            /*BackgroundJob.Schedule(
                () => FleetboardSeeder
                .DataSeed(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 00, 00, 00), DateTime.Now),
                TimeSpan.FromMinutes(3));*/
        }
    }
}
