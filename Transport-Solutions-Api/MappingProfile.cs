﻿using AutoMapper;
using Entities.DataTransferObjects;
using Entities.DataTransferObjects.Creation;
using Entities.DataTransferObjects.Dto;
using Entities.Models;

namespace Transport_Solutions_Api
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<FleetUser, FleetUserDto>();
            CreateMap<FleetUserCreationDto, FleetUser>();

            CreateMap<Fleet, FleetDto>();
            CreateMap<FleetCreationDto, Fleet>();
            CreateMap<Fleet, FleetCreationDto>();

            CreateMap<FleetProperties, FleetPropertiesDto>();
            CreateMap<FleetPropertiesCreationDto, FleetProperties>();
            CreateMap<FleetProperties, FleetPropertiesCreationDto>();

            CreateMap<FleetRights, FleetRightsDto>();
            CreateMap<FleetRightsCreationDto, FleetRights>();
            CreateMap<FleetRights, FleetRightsCreationDto>();

            CreateMap<Vehicle, VehicleDto>();
            CreateMap<VehicleCreationDto, Vehicle>();
            CreateMap<Vehicle, VehicleCreationDto>();

            CreateMap<VehicleGroup, VehicleGroupDto>();
            CreateMap<VehicleGroup, VehicleGroupCreationDto>();
            CreateMap<VehicleGroupCreationDto, VehicleGroup>();

            CreateMap<VehicleHardware, VehicleHardwareDto>();
            CreateMap<VehicleHardware, VehicleHardwareCreationDto>();
            CreateMap<VehicleHardwareCreationDto, VehicleHardware>();

            CreateMap<VehicleProperties, VehiclePropertiesDto>();
            CreateMap<VehicleProperties, VehiclePropertiesCreationDto>();
            CreateMap<VehiclePropertiesCreationDto, VehicleProperties>();

            CreateMap<TripRecord, TripRecordDto>();
            CreateMap<TripRecord, TripRecordCreationDto>();
            CreateMap<TripRecordCreationDto, TripRecord>();

            CreateMap<VehicleInfo, VehicleInfoDto>();
            CreateMap<VehicleInfo, VehicleInfoCreationDto>();
            CreateMap<VehicleInfoCreationDto, VehicleInfo>();

            CreateMap<TripStart, TripStartDto>();
            CreateMap<TripStart, TripStartCreationDto>();
            CreateMap<TripStartCreationDto, TripStart>();

            CreateMap<TripEnd, TripEndDto>();
            CreateMap<TripEnd, TripEndCreationDto>();
            CreateMap<TripEndCreationDto, TripEnd>();

            CreateMap<Position, PositionDto>();
            CreateMap<Position, PositionCreationDto>();
            CreateMap<PositionCreationDto, Position>();
        }
    }
}
