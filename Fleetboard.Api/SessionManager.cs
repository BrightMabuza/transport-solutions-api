﻿using System;
namespace Fleetboard.Api
{
    public class SessionManager
    {
        #region Singleton Pattern

        private readonly static SessionManager INSTANCE = new SessionManager();

        public static SessionManager Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        #endregion

        #region Fields

        private string _sessionId;

        #endregion

        #region Properties

        public string SessionId
        {
            get
            {
                return _sessionId;
            }
        }

        #endregion

        #region Events

        public event EventHandler<SessionIdEventArgs> OnSessionIdChanged;

        #endregion

        #region Constructor

        private SessionManager()
        {
        }

        #endregion

        #region Methods

        public void UpdateSessionId(string sessionId)
        {
            _sessionId = sessionId;
            var tmp = OnSessionIdChanged;
            if (tmp != null)
                tmp(this, new SessionIdEventArgs(sessionId));
        }

        #endregion
    }
}
