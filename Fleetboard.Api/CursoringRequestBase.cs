﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Fleetboard.Api
{
    public abstract class CursoringRequestBase<T>
    {
        #region Methods

        public T GetSingleResult()
        {
            SetOffsetAndLimit("0", "1");

            Execute();

            var result = GetResult();
            if (result == null)
                throw new Exception("No result");
            return result.FirstOrDefault();
        }

        public IEnumerable<T> GetAllResults()
        {
            int offset = 0;
            int? limit = null;
            int receivedItems = 0;

            while (true)
            {
                string offsetString = offset.ToString(CultureInfo.InvariantCulture);
                string limitString = limit.HasValue ? limit.Value.ToString(CultureInfo.InvariantCulture) : null;
                SetOffsetAndLimit(offsetString, limitString);

                // invoke the webservice
                Execute();

                string responseSizeString = GetResponseSize();
                int responseSize;
                bool responseSizeParsed = int.TryParse(responseSizeString, out responseSize);

                if (!responseSizeParsed || responseSize == 0)
                    yield break;

                string resultSizeString = GetResultSize();
                int resultSize;
                bool resultSizeParsed = int.TryParse(resultSizeString, out resultSize);

                if (!resultSizeParsed)
                    yield break;

                string responseLimitString = GetResponseLimit();
                int responseLimit;
                bool limitParsed = int.TryParse(responseLimitString, out responseLimit);

                if (!limitParsed)
                    yield break;

                limit = responseLimit;

                foreach (var result in GetResult())
                {
                    receivedItems++;
                    yield return result;
                }

                if (receivedItems >= resultSize)
                    yield break;

                offset += responseSize;
            }
        }

        public IEnumerable<T> GetAllResultsWithDateRange(DateTime begin, DateTime end, TimeSpan maxSpan)
        {
            var dateRanges = SplitDateRange(begin, end, maxSpan);

            foreach (var dateRange in dateRanges)
            {
                SetDate(dateRange.Item1, dateRange.Item2);
                foreach (var result in GetAllResults())
                {
                    yield return result;
                }
            }
        }

        protected abstract void SetOffsetAndLimit(string offset, string limit);

        protected abstract IEnumerable<T> GetResult();

        protected abstract string GetResponseSize();

        protected abstract string GetResponseLimit();

        protected abstract string GetResultSize();

        protected abstract void SetDate(DateTime begin, DateTime end);

        protected abstract void Execute();

        private IEnumerable<Tuple<DateTime, DateTime>> SplitDateRange(DateTime begin, DateTime end, TimeSpan maxSpan)
        {
            if (maxSpan < TimeSpan.Zero)
            {
                yield return new Tuple<DateTime, DateTime>(begin, end);
            }
            else
            {
                var current = begin;

                while (current + maxSpan < end)
                {
                    yield return new Tuple<DateTime, DateTime>(current, current + maxSpan);

                    current += maxSpan;
                }
                yield return new Tuple<DateTime, DateTime>(current, end);
            }
        }

        #endregion
    }
}
