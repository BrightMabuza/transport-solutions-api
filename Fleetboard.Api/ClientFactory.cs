﻿using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace Fleetboard.Api
{
    public class ClientFactory
    {
        public static T CreateClient<T>(string url)
        {
            var binding = new BasicHttpBinding
            {
                AllowCookies = false,
                MaxReceivedMessageSize = 10 * 1024 * 1024, // 10 megabytes
            };

            binding.Security.Mode = BasicHttpSecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var factory = new ChannelFactory<T>(binding, new EndpointAddress(url));

            factory.Endpoint.EndpointBehaviors.Add(new EndpointBehavior());

            return factory.CreateChannel();
        }

        public static T CreateClient<T>(string url, string sessionId)
        {
            return CreateClient<T>(url + ";jsessionid=" + sessionId);
        }

        public static void Close<T>(T channel)
        {
            IClientChannel clientChannel = (IClientChannel)channel;
            clientChannel.Close();
        }

        private class EndpointBehavior : IEndpointBehavior
        {
            public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
            {
            }

            public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
            {
            }

            public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
            {
            }

            public void Validate(ServiceEndpoint endpoint)
            {
            }
        }
    }
}
