﻿using System;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Fleetboard.Api.ApplicationLayer.Exceptions
{
    public class FleetboardFaultException : Exception
    {
        private readonly FaultDetails _details;

        public const int ERROR_INVALID_CREDENTIALS = 1007002;

        public FaultDetails Details
        {
            get
            {
                return _details;
            }
        }

        public FleetboardFaultException(FaultDetails details, FaultException faultException)
            : base(details.Message, faultException)
        {
            _details = details;
        }

        public static FleetboardFaultException Create(FaultException faultException)
        {
            var msgFault = faultException.CreateMessageFault();
            if (!msgFault.HasDetail)
                return null;

            var details = msgFault.GetDetail<FaultDetails>();
            return new FleetboardFaultException(details, faultException);
        }

        protected FleetboardFaultException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}
