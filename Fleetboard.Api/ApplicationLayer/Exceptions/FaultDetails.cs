﻿using System;
using System.Runtime.Serialization;

namespace Fleetboard.Api.ApplicationLayer.Exceptions
{
    [DataContract(Name = "FbFaultDetails", Namespace = "http://www.fleetboard.com/data")]
    public class FaultDetails
    {
        [DataMember(Name = "errorcode")]
        public int ErrorCode { get; set; }

        [DataMember(Name = "message")]
        public string Message { get; set; }
    }
}
