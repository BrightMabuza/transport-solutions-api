﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using BasicServiceReference;
using Contracts;
using Entities.DataTransferObjects;
using Entities.DataTransferObjects.Creation;
using Fleetboard.Api.ApplicationLayer.Exceptions;
using Fleetboard.Api.ApplicationLayer.Helper.Request;

namespace Fleetboard.Api.ApplicationLayer.ServiceFacade
{
    public class FleetServiceFacade : IFleetServiceFacade
    {
        private BasicService _basicService;
        private string _webserviceUrl = "https://www.fleetboard.com/soap_v1_1/services/BasicService";

        public FleetServiceFacade()
        {
            _basicService = ClientFactory.CreateClient<BasicService>(_webserviceUrl, SessionManager.Instance.SessionId);
        }

        public FleetCreationDto GetFleet(long fleetId)
        {
            try
            {
                var request = new FleetRequest(
                    _basicService,
                    new getFleetRequest1(
                        new getFleet()
                        {
                            GetFleetRequest = new GetFleetRequest()
                            {
                                FleetId = fleetId,
                                FleetIdSpecified = true,
                                version = 10101,
                                limit = "1000",
                                offset = "0",
                                versionSpecified = true,
                            }
                        }))
                    .GetAllResults();

                   var res =  request.FirstOrDefault();

              FleetCreationDto  fleet = new FleetCreationDto
                {
                    FleetId = res.FLEETID,
                    FleetName = res.FLEETNAME,
                    City = res.FLEETINFORMATION.CITY,
                    Country = res.FLEETINFORMATION.COUNTRY,
                    CountryCode = res.FLEETINFORMATION.COUNTRYCODE,
                    DeltaAmon = res.FLEETINFORMATION.DELTAETAMON,
                    DeltaAreaAmon = res.FLEETINFORMATION.DELTAAREAMON,
                    DlSaving = res.FLEETINFORMATION.DLSAVING,
                    InoId = res.inoid,
                    FleetTimeZone = res.FLEETINFORMATION.FLEETTIMEZONE,
                    FleetTimeZoneId = res.FLEETINFORMATION.FLEETTIMEZONEID,
                    PostalCode = res.FLEETINFORMATION.POSTALCODE,
                    Region = res.FLEETINFORMATION.REGION,
                    Street = res.FLEETINFORMATION.STREET,
                    PageHeading = res.FLEETINFORMATION.PAGEHEADING,
                    FleetProperties = res.FLEETPROPERTIES.Select(e => new FleetPropertiesCreationDto
                    {
                        Name = e.Name,
                        Value = e.Value
                    }).ToList(),
                    FleetRights = res.FLEETRIGHTS.Select(e => new FleetRightsCreationDto
                    {
                        Name = e.NAME,
                        Type = e.TYPE,
                        Service = e.SERVICE
                    }).ToList()
                };

                return fleet;
            }
            catch (FaultException ex)
            {
                var fleetboardException = FleetboardFaultException.Create(ex);

                if (fleetboardException != null)
                    throw fleetboardException;
                else
                    throw;
            }
        }

        public IEnumerable<VehicleGroupCreationDto> GetVehicleGroups()
        {
            try
            {
                var request = _basicService.getVehicleGroupAsync(
                   new getVehicleGroupRequest1(
                       new getVehicleGroup
                       {
                           GetVehicleGroupRequest = new GetVehicleGroupRequest()
                       })).Result;

                var response = request.getVehicleGroupResponse.GetVehicleGroupResponse.VEHICLEGROUP;

                var res = response.ToList()
                    .Select(res =>
                        new VehicleGroupCreationDto
                        {
                            GroupId = res.GROUPID,
                            GroupName = res.GROUPNAME,
                            Description = res.Description,
                            FleetId = res.FLEETID,
                            InoId = res.inoid
                        });

                return res;
            }
            catch (FaultException ex)
            {
                var fleetboardException = FleetboardFaultException.Create(ex);

                if (fleetboardException != null)
                    throw fleetboardException;
                else
                    throw;
            }
        }

        public List<VehicleCreationDto> GetVehicles()
        {
            try
            {
                var request = new VehicleRequest(
                   _basicService,
                   new getVehicleRequest1(
                       new getVehicle()
                       {
                           GetVehicleRequest = new GetVehicleRequest()
                           {
                               version = 010500,
                               versionSpecified = true,
                           }
                       })).GetAllResults().ToList();

                List<VehicleCreationDto> response = request.Select(data =>
                        new VehicleCreationDto()
                        {
                            Chassis = data.CHASSIS,
                            EndTime = data.FLEETVEHICLE.ENDTIME,
                            FleetId = data.FLEETVEHICLE.FLEETID,
                            INSMSISDN = data.FLEETVEHICLE.INSMSISDN,
                            Registration = data.FLEETVEHICLE.REGISTRATION,
                            RegistrationDate = data.REGISTRATIONDATE,
                            StartTime = data.FLEETVEHICLE.STARTTIME,
                            Status = data.FLEETVEHICLE.STATUS,
                            TelematicGroup = (int)data.TELEMATICGROUP,
                            TelematicGroupSpecified = data.TELEMATICGROUPSpecified,
                            VehicleNumber = data.FLEETVEHICLE.VEHICLENUMBER,
                            VehicleType = data.FLEETVEHICLE.VEHICLETYPE,
                            InoId = data.inoid,
                            VehicleHardware = data.VEHICLEHARDWARE.Select(h =>
                            new VehicleHardwareCreationDto
                            {
                                Hardware = h.HARDWARE,
                                HardwareId = h.HARDWAREID
                            }).ToList(),
                            VehicleProperties = data.VEHICLEPROPERTIES.Select(p =>
                            new VehiclePropertiesCreationDto
                            {
                                Name = p.Name,
                                Value = p.Value
                            })
                                    .ToList(),
                            VehicleGroupId = string.Join(",", data.GROUPVEHICLE.Select(p => p.ToString()).ToArray())
                        }).ToList();

                return response;
            }
            catch (FaultException ex)
            {
                var fleetboardException = FleetboardFaultException.Create(ex);

                if (fleetboardException != null)
                    throw fleetboardException;
                else
                    throw;
            }
        }
    }
}
