﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using Contracts;
using Entities.DataTransferObjects.Creation;
using Fleetboard.Api.ApplicationLayer.Exceptions;
using Fleetboard.Api.ApplicationLayer.Helper.Request;
using TripRecordServiceReference;

namespace Fleetboard.Api.ApplicationLayer.ServiceFacade
{
    public class TripRecordServiceFacade : ITripRecordServiceFacade
    {
        private string _webserviceUrl = " https://www.fleetboard.com/vmsoap_v1_1/services/TripRecordService";
        private readonly TripRecordServiceInterface _tripRecordService;

        public TripRecordServiceFacade()
        {
            _tripRecordService = ClientFactory.CreateClient<TripRecordServiceInterface>(_webserviceUrl, SessionManager.Instance.SessionId);
        }

        public List<TripRecordCreationDto> GetTripRecords(long vehicleId, DateTime begin, DateTime end)
        {
            try
            {
                var request = new TripRecordRequest(
                    _tripRecordService,
                    new getTripRecordRequest(
                        new getTripRecord
                        {
                            GetTripRecordRequest = new GetTripRecordRequestType()
                        }),
                    begin, end, vehicleId);

                List<TripRecordCreationDto> trip = request.GetAllResults()
                    .Select(res =>
                         new TripRecordCreationDto
                            {
                                TripId = res.ID,
                                TripRecordKind = res.TripRecordKind.ToString(),
                                DriverNameId = res.DriverNameID,
                                DriverNameIdSpecified = (bool)res.DiscChangesSpecified,
                                Consumption = res.Consumption,
                                ConsumptionSpecified = (bool)res.ConsumptionSpecified,
                                DiscChanges = res.DiscChanges,
                                DiscChangesSpecified = (bool)res.DiscChangesSpecified,
                                TruckWeight = res.TruckWeight,
                                TruckWeightSpecified = (bool)res.TruckWeightSpecified,
                                FuelLevel = res.FuelLevel,
                                FuelLevelSpecified = (bool)res.FuelLevelSpecified,
                                AdBlueConsumption =res.AdBlueConsumption,
                                AdBlueConsumptionSpecified = (bool)res.AdBlueConsumptionSpecified,
                                AdBlueLevel = res.AdBlueLevel,
                                AdBlueLevelSpecified = (bool)res.AdBlueLevelSpecified,
                                AdBlueToFuelRatioConsumption = res.AdBlueToFuelRatioConsumption,
                                AdBlueToFuelRatioConsumptionSpecified = (bool)res.AdBlueToFuelRatioConsumptionSpecified,
                                VehicleInfo = new VehicleInfoCreationDto
                                {
                                    vehicleId = res.VehicleInfo.ID,
                                    IgnitionStatus = res.VehicleInfo.IgnitionStatus.ToString(),
                                    IgnitionStatusSpecified = (bool)res.VehicleInfo.IgnitionStatusSpecified,
                                    MotorStatus = res.VehicleInfo.MotorStatus.ToString(),
                                    MotorStatusSpecified = (bool)res.VehicleInfo.MotorStatusSpecified,
                                    TrailerName = res.VehicleInfo.TrailerName
                                },
                                Start = new TripStartCreationDto
                                {
                                    Mileage = res.Start.Mileage,
                                    VehicleTimeStamp = res.Start.VehicleTimestamp,
                                    Position = new PositionCreationDto
                                    {
                                        Long = res.Start.Position.Long,
                                        Course = res.Start.Position.Course,
                                        GpsStatus = res.Start.Position.Course,
                                        KM = res.Start.Position.KM,
                                        Lat = res.Start.Position.Lat,
                                        LatSpecified = (bool)res.Start.Position.LatSpecified,
                                        LongSpecified = (bool)res.Start.Position.LongSpecified,
                                        PosText = res.Start.Position.PosText,
                                        Speed = res.Start.Position.Speed
                                    }
                                },
                                End = new TripEndCreationDto
                            {
                                Mileage = res.End.Mileage,
                                VehicleTimeStamp = res.End.VehicleTimestamp,
                                Position = new PositionCreationDto
                                {
                                    Long = res.End.Position.Long,
                                    Course = res.End.Position.Course,
                                    GpsStatus = res.End.Position.Course,
                                    KM = res.End.Position.KM,
                                    Lat = res.End.Position.Lat,
                                    LatSpecified = (bool)res.End.Position.LatSpecified,
                                    LongSpecified = (bool)res.End.Position.LongSpecified,
                                    PosText = res.End.Position.PosText,
                                    Speed = res.End.Position.Speed
                                }
                            }

                }).ToList();
                return trip;
            }
            catch (FaultException ex)
            {
                var fleetboardException = FleetboardFaultException.Create(ex);

                if (fleetboardException != null)
                    throw fleetboardException;
                throw;
            }
        }
    }
}
