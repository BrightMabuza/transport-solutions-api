﻿using System.ServiceModel;
using BasicServiceReference;
using Contracts;
using Entities.Models;
using Fleetboard.Api.ApplicationLayer.Exceptions;

namespace Fleetboard.Api.ApplicationLayer.ServiceFacade
{
    public class AuthServiceFacade : IAuthServiceFacade
    {
        private BasicService _basicService;
        private string _webserviceUrl = "https://www.fleetboard.com/soap_v1_1/services/BasicService";

        public AuthServiceFacade()
        {
            _basicService = ClientFactory.CreateClient<BasicService>(_webserviceUrl);

            SessionManager.Instance.OnSessionIdChanged += delegate (object sender, SessionIdEventArgs args)
            {
                _basicService = ClientFactory.CreateClient<BasicService>(_webserviceUrl, args.SessionId);
            };
        }

        public object ServiceLog { get; private set; }

        public string Login(string fleet, string username, string password)
        {
            string sessionId = null;
            _basicService = ClientFactory.CreateClient<BasicService>(_webserviceUrl);

            try
            {
                var login = _basicService.loginAsync(
                    new loginRequest1(
                        new login
                        {
                           LoginRequest =  new LoginRequest
                            {
                                Fleetname = fleet.ToUpper(),
                                User = username,
                                Password = password
                            }
                        }))
                    .Result;

                sessionId = login.loginResponse.LoginResponse.sessionid;

                SessionManager.Instance.UpdateSessionId(sessionId);

                return sessionId;
            }
            catch (FaultException ex)
            {
                var fleetboardException = FleetboardFaultException.Create(ex);

                if (fleetboardException != null)
                    throw fleetboardException;
                else
                    throw;
            }
        }

        public FleetUser GetCurrentUser()
        {

            var response = _basicService.getCurrentUserAsync(
                new getCurrentUserRequest1(
                    new getCurrentUser
                    {
                        GetCurrentUserRequest = new GetCurrentUserRequest()
                        {
                            version = 10500,
                            versionSpecified = true
                        }
                    })).Result;

            var user = response.getCurrentUserResponse.GetCurrentUserResponse.User;

            return new FleetUser {FleetId = user.UserSettings.FleetID };
        }

        public bool Logout()
        {
            try
            {
                var logout = _basicService.logoutAsync(
                    new logoutRequest1(
                        new logout
                        {
                            LogoutRequest = new LogoutRequest()
                        }))
                    .Result;

                SessionManager.Instance.UpdateSessionId(null);

                return logout.logoutResponse.LogoutResponse.OK != null;
            }
            catch (FaultException ex)
            {
                var fleetboardException = FleetboardFaultException.Create(ex);

                if (fleetboardException != null)
                    throw fleetboardException;
                else
                    throw;
            }
        }

        public void Close()
        {
            ClientFactory.Close(_basicService);
        }
    }
}
