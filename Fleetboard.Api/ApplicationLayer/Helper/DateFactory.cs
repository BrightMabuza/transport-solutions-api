﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using BasicServiceReference;
using TripRecordServiceReference;

namespace Fleetboard.Api.ApplicationLayer.Helper
{
    public class DateFactory
    {
        // The date format as expected by the webservice
        private const string DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

        public static TPDate CreateTripRecordDate(DateTime? begin, DateTime? end)
        {
            if (!(begin.HasValue && end.HasValue))
                return null;

            return new TPDate { Begin = Format(begin.Value), End = Format(end.Value) };
        }

        public static TimeSpan ParsePeriodQueryLimit(IEnumerable<PropertyType> properties, string propertyName)
        {
            try
            {
                var property = properties.First(p => p.Name == propertyName);
                return TimeSpan.FromHours(int.Parse(property.Value));
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to parse period query limit " + propertyName, ex);
            }
        }

        public static DateTime Parse(string dateTime)
        {
            return DateTime.Parse(dateTime, CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal).ToUniversalTime();
        }

        private static string Format(DateTime dateTime)
        {
            return dateTime.ToString(DATE_FORMAT);
        }
    }
}
