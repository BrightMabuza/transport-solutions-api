﻿using System;
using System.Collections.Generic;
using BasicServiceReference;

namespace Fleetboard.Api.ApplicationLayer.Helper.Request
{
    public class VehicleRequest : CursoringRequestBase<VEHICLE>
    {
        private readonly BasicService _basicService;
        private readonly getVehicleRequest1 _request;
        private getVehicleResponse1 _response;

        public VehicleRequest(BasicService basicService, getVehicleRequest1 request)
        {
            _basicService = basicService;
            _request = request;
        }

        protected override void SetDate(DateTime begin, DateTime end)
        {
        }

        protected override void Execute()
        {
            _response = _basicService.getVehicleAsync(_request).GetAwaiter().GetResult();
        }

        protected override void SetOffsetAndLimit(string offset, string limit)
        {
            _request.getVehicle.GetVehicleRequest.offset = offset;
            _request.getVehicle.GetVehicleRequest.limit = limit;
        }

        protected override IEnumerable<VEHICLE> GetResult()
        {
            return _response.getVehicleResponse.GetVehicleResponse.VEHICLE;
        }

        protected override string GetResponseSize()
        {
            return _response.getVehicleResponse.GetVehicleResponse.responseSize;
        }

        protected override string GetResultSize()
        {
            return _response.getVehicleResponse.GetVehicleResponse.resultSize;
        }

        protected override string GetResponseLimit()
        {
            return _response.getVehicleResponse.GetVehicleResponse.limit;
        }
    }
}
