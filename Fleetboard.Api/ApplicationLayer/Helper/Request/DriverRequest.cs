﻿using System;
using System.Collections.Generic;
using BasicServiceReference;

namespace Fleetboard.Api.ApplicationLayer.Helper.Request
{
    public class DriverRequest : CursoringRequestBase<GetDriverResponseTypeDriverInfo>
    {
        private readonly BasicService _service;
        private readonly getDriverRequest _request;
        private getDriverResponse1 _response;

        public DriverRequest(BasicService vehicleService, getDriverRequest request)
        {
            _service = vehicleService;
            _request = request;
        }

        protected override void SetDate(DateTime begin, DateTime end)
        {
        }

        protected override void Execute()
        {
            _response = _service.getDriverAsync(_request).GetAwaiter().GetResult();
        }

        protected override void SetOffsetAndLimit(string offset, string limit)
        {
            _request.getDriver.GetDriverRequest.offset = offset;
            _request.getDriver.GetDriverRequest.limit = limit;
        }

        protected override IEnumerable<GetDriverResponseTypeDriverInfo> GetResult()
        {
            return _response.getDriverResponse.GetDriverResponse.DriverInfo;
        }

        protected override string GetResponseSize()
        {
            return _response.getDriverResponse.GetDriverResponse.responseSize;
        }

        protected override string GetResultSize()
        {
            return _response.getDriverResponse.GetDriverResponse.resultSize;
        }

        protected override string GetResponseLimit()
        {
            return _response.getDriverResponse.GetDriverResponse.limit;
        }
    }
}
