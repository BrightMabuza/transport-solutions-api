﻿using System;
using System.Collections.Generic;
using BasicServiceReference;

namespace Fleetboard.Api.ApplicationLayer.Helper.Request
{
    public class FleetRequest : CursoringRequestBase<FLEET>
    {
        private readonly BasicService _basicService;
        private readonly getFleetRequest1 _request;
        private getFleetResponse1 _response;

        public FleetRequest(BasicService basicService, getFleetRequest1 request)
        {
            _request = request;
            _basicService = basicService;
        }

        protected override void SetDate(DateTime begin, DateTime end)
        {
        }

        protected override void Execute()
        {
            _response = _basicService.getFleetAsync(_request).GetAwaiter().GetResult();
        }

        protected override void SetOffsetAndLimit(string offset, string limit)
        {
            _request.getFleet.GetFleetRequest.offset = offset;
            _request.getFleet.GetFleetRequest.limit = limit;
        }

        protected override IEnumerable<FLEET> GetResult()
        {
            yield return _response.getFleetResponse.GetFleetResponse.FLEET;
        }

        protected override string GetResponseSize()
        {
            return _response.getFleetResponse.GetFleetResponse.responseSize;
        }

        protected override string GetResultSize()
        {
            return _response.getFleetResponse.GetFleetResponse.resultSize;
        }

        protected override string GetResponseLimit()
        {
            return _response.getFleetResponse.GetFleetResponse.limit;
        }
    }
}
