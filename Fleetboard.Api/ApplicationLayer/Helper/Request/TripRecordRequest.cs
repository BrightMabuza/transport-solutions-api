﻿using System;
using System.Collections.Generic;
using TripRecordServiceReference;

namespace Fleetboard.Api.ApplicationLayer.Helper.Request
{
    public class TripRecordRequest : CursoringRequestBase<GetTripRecordResponseTypeTripRecordReport>
    {
        private readonly TripRecordServiceInterface _tripRecordService;
        private readonly getTripRecordRequest _request;
        private getTripRecordResponse1 _response;

        public TripRecordRequest(TripRecordServiceInterface tripRecordService, getTripRecordRequest request, DateTime begin, DateTime end, long vehicleId)
        {
            _tripRecordService = tripRecordService;
            _request = request;

            SetDate(begin, end);
            SetVehicle(vehicleId);
        }

        private void SetVehicle(long vehicleId)
        {
            _request.getTripRecord.GetTripRecordRequest.VehicleID = vehicleId;
            _request.getTripRecord.GetTripRecordRequest.VehicleIDSpecified = true;
        }

        protected override void Execute()
        {
            _response = _tripRecordService.getTripRecordAsync(_request).GetAwaiter().GetResult();
        }

        protected override string GetResponseLimit()
        {
            return _response.getTripRecordResponse.GetTripRecordResponse.limit;
        }

        protected override string GetResponseSize()
        {
            return _response.getTripRecordResponse.GetTripRecordResponse.responseSize;
        }

        protected override IEnumerable<GetTripRecordResponseTypeTripRecordReport> GetResult()
        {
            return _response.getTripRecordResponse.GetTripRecordResponse.TripRecordReport;
        }

        protected override string GetResultSize()
        {
            return _response.getTripRecordResponse.GetTripRecordResponse.responseSize;
        }

        protected override void SetDate(DateTime begin, DateTime end)
        {
            // Set the time
            TimeRangeType time = new TimeRangeType();
            time.Model = TimeRangeTypeModel.PERIOD;
            time.ModelSpecified = true;

            time.Period = DateFactory.CreateTripRecordDate(begin, end);

            _request.getTripRecord.GetTripRecordRequest.TimeRange = time;
        }

        protected override void SetOffsetAndLimit(string offset, string limit)
        {
            _request.getTripRecord.GetTripRecordRequest.offset = offset;
            _request.getTripRecord.GetTripRecordRequest.limit = limit;
        }
    }
}
