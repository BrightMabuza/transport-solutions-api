﻿using System;
namespace Fleetboard.Api
{
    public class SessionIdEventArgs : EventArgs
    {
        public string SessionId { get; set; }

        public SessionIdEventArgs(string sessionid)
        {
            SessionId = sessionid;
        }
    }
}
