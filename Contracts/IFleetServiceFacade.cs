﻿using System.Collections.Generic;
using Entities.DataTransferObjects;
using Entities.DataTransferObjects.Creation;

namespace Contracts
{
    public interface IFleetServiceFacade
    {
        FleetCreationDto GetFleet(long fleetId);
        IEnumerable<VehicleGroupCreationDto> GetVehicleGroups();
        List<VehicleCreationDto> GetVehicles();
    }
}
