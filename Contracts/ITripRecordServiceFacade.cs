﻿using System;
using System.Collections.Generic;
using Entities.DataTransferObjects.Creation;

namespace Contracts
{
    public interface ITripRecordServiceFacade
    {
        List<TripRecordCreationDto> GetTripRecords(long vehicleId, DateTime begin, DateTime end);
    }
}
