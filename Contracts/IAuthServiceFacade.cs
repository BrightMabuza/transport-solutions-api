﻿using Entities.Models;

namespace Contracts
{
    public interface IAuthServiceFacade
    {
        string Login(string fleet, string username, string password);
        FleetUser GetCurrentUser();
        bool Logout();
    }
}
