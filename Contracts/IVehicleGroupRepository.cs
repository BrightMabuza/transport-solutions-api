﻿using System.Collections.Generic;
using Entities.DataTransferObjects.Creation;
using Entities.Models;

namespace Contracts
{
    public interface IVehicleGroupRepository : IRepositoryBase<VehicleGroup>
    {
        IEnumerable<VehicleGroupCreationDto> GetVehicleGroups();
        void CreateVehicleGroup(VehicleGroup vehicleGroup);
        List<VehicleGroup> GetAllVehiclesGroups();
        VehicleGroup GetVehicleGroupById(string groupId);
        void UpdateVehicleGroup(VehicleGroup vehicleGroup);
        void DeleteVehicleGroup(VehicleGroup vehicleGroup);
    }
}
