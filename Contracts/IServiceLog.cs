﻿namespace Contracts
{
    public interface IServiceLog
    {
        void LogStart(string service, string operation, string format, string SessionId, params object[] args);
        void LogEnd(string service, string operation, string format, string SessionId, params object[] args);
    }
}
