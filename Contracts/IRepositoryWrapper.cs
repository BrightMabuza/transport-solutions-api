﻿namespace Contracts
{
    public interface IRepositoryWrapper
    {
        IFleetUserRepository FleetUser { get; }
        IFleetServiceRepository Fleet { get; }
        IVehicleServiceRepository Vehicle { get; }
        IVehicleGroupRepository VehicleGroup { get; }
        ITripRecordServiceRepository TripRecord { get; }
        void Save();
    }
}
