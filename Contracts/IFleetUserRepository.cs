﻿using System;
using System.Collections.Generic;
using Entities.Models;

namespace Contracts
{
    public interface IFleetUserRepository : IRepositoryBase<FleetUser>
    {
        IEnumerable<FleetUser> GetAllFleetUsers();
        FleetUser GetFleetUserById(long Id);
        void PostFleetUser(FleetUser fleetUser);
        void UpdateFleet(FleetUser fleetUser);
        void DeleteFleetUser(FleetUser fleetUser);
    }
}
