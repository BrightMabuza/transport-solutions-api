﻿using System.Collections.Generic;
using Entities.DataTransferObjects;
using Entities.Models;

namespace Contracts
{
    public interface IVehicleServiceRepository : IRepositoryBase<Vehicle>
    {
        List<VehicleCreationDto> GetVehicle();
        List<Vehicle> GetAllVehicles();
        List<Vehicle> GetVehicleByFleetId(long fleetId);
        Vehicle GetVehicleByVehicleId(long vehicleId);
        Vehicle GetVehicleWithDetails(string chassis);
        void CreateVehicle(Vehicle vehicle);
        void UpdateVehicle(Vehicle vehicle);
        void DeleteVehicle(Vehicle vehicle);
    }
}
