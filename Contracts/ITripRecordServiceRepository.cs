﻿using System;
using System.Collections.Generic;
using Entities.DataTransferObjects.Creation;
using Entities.Models;

namespace Contracts
{
    public interface ITripRecordServiceRepository : IRepositoryBase<TripRecord>
    {
        List<TripRecordCreationDto> GetTripRecord(long vehicleId, DateTime begin, DateTime end);
        List<TripRecord> GetAllTripRecords();
        TripRecord GetTripRecordById(long tripId);
        TripRecord GetTripRecordWithDetails(long tripId);
        void CreateTripRecord(TripRecord tripRecord);
        void UpdateTripRecord(TripRecord tripRecord);
        void DeleteTripRecord(TripRecord tripRecord);
    }
}
