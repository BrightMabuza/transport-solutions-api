﻿using System.Collections.Generic;
using Entities.DataTransferObjects;
using Entities.Models;

namespace Contracts
{
    public interface IFleetServiceRepository : IRepositoryBase<Fleet>
    {
        List<Fleet> GetAllFleets();
        FleetCreationDto GetFleet(long fleetId);
        Fleet GetFleetById(long tripId);
        Fleet GetFleetWithDetails(long tripId);
        void CreateFleet(Fleet fleet);
        void UpdateFleet(Fleet fleet);
        void DeleteFleet(Fleet fleet);
    }
}
