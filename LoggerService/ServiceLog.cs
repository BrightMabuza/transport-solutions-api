﻿using Contracts;

namespace LoggerService
{
    public class ServiceLog : IServiceLog
    {
        private ILoggerManager _logger;

        public ServiceLog(ILoggerManager logger)
        {
            _logger = logger;
        }
        public void LogEnd(string service, string operation, string format, string SessionId, params object[] args)
        {
            string argumentString = string.Format(format, args);
            string argumentSeparator = argumentString == "" ? "" : ", ";

            _logger.LogInfo($"{ service } { operation } { SessionId } { argumentSeparator } { argumentString } ");
        }

        public void LogStart(string service, string operation, string format,string SessionId, params object[] args)
        {
            string resultString = string.Format(format, args);
            _logger.LogInfo($"{ service } { operation } { SessionId } { resultString }");
        }
    }
}
